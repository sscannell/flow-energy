﻿Title: About Flow

----

Text: # Who are Flow?

----

Box_1_image: http:/assets/images/flow-union-flag.png

----

Box_1_image_alt: 

----

Box_1_description: ## A UK energy technology company

Flow is a UK company.  Our energy team is based in Ipswich.  Our research and development facility is in Capenhurst, near Chester.  We build exciting energy products - like the Flow microCHP boiler, designed to lower customer bills and reduce carbon emissions. So we're not just another energy company, selling energy to hard-pressed customers and turning a profit. We thrive on bringing benefits to energy customers through innovation.

----

Box_1_button_text: 

----

Box_1_button_link: 

----

Box_2_image: http:/images/flow-whygowithflow-brain.png

----

Box_2_image_alt: 

----

Box_2_description: ## Years of experience

We have some of the industry's leading experts at the top of our energy business. These are people with the knowledge, insight and experience to deliver an outstanding service to you. And our world-leading engineers are just as impressive.  Their understanding of energy technology is second-to-none.  That's why they've been able to develop the Flow boiler, a boiler so clever it pays for itself.  So can you trust Flow to deliver?  Absolutely.

----

Box_2_button_text: 

----

Box_2_button_link: 

----

Box_3_image: http:/assets/images/flow-keep-it-simple.png

----

Box_3_image_alt: 

----

Box_3_description: ## Keeping it simple

Flow believes you want a simpler experience from your energy company. So instead of complicated bills, we have clear bills. Instead of complex tariffs, we had just one at launch.  Instead of being passed from team to team when you need an answer, you’ll be put straight through, when you call Flow, to someone with the experience and authority to help you straight away.  That's another thing that makes Flow different.

----

Box_3_button_text: 

----

Box_3_button_link: 

----

Box_4_image: http:/assets/images/flow-green.png

----

Box_4_image_alt: 

----

Box_4_description: ## Is Flow green?

Flow doesn’t currently offer a green tariff.  Instead, we’re green in a much more important way.  The Flow boiler employs patented technology to generate electricity as it uses gas to heat a home.  This reduces a customer’s electricity bill and saves the average household around 1000kg of CO2 annually.  When we have hundreds of thousands of Flow boilers in operation, they’ll make a significant contribution to reducing the UK’s carbon emissions – and customer bills.  Flow is a great example of The New Green – using cutting-edge technology to help you make a difference and save you money.

----

Box_4_button_text: 

----

Box_4_button_link: 

----

Box_5_image: http:/images/Flowgroup-logo.jpg

----

Box_5_image_alt: 

----

Box_5_description: ##Part of a Plc

Flow Energy is part of Flowgroup - one of the fastest growing Energy Services organisations in Great Britain. Flowgroup has been around since 1997 and specialises in developing and commercialising products to meet the growing global demand for energy - like the revolutionary Flow microCHP, electricity-generating boiler.  So, with Flow you get the innovation and friendly feel of a smaller company and the resources and reputation of a Plc.

(link:http://flowgroup.uk.com/ text: Find out more about Flowgroup popup:yes class:button blue)

----

Box_5_button_text: 

----

Box_5_button_link: 

----

Meta_description: Flow Energy is a UK energy company that you can trust

----

Meta_description_description: Flow Energy is a UK energy company based in Suffolk.  We're part of Flowgroup - one of the fastest growing Energy Services organisations in Great Britain. Flowgroup specialises in developing and commercialising products to meet the growing global demand for energy.