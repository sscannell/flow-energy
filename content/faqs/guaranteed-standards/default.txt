﻿Title: Guaranteed Standards

----

Text: # Guaranteed Standards
We always aim to provide you with the best possible service, as part of our promise to you.  Below you'll find a list of the guaranteed standards we always aim to work to - and an explanation of what we'll do if we don't meet those standards.

These standards were last updated on 2 April 2013.

### Meter Accuracy
If you think that your meter is not reading accurately we'll investigate and get back to you within 5 working days. If you don't hear from us in that time, we'll pay you &pound;22 if your concern relates to an electricity meter or &pound;20 for a gas meter, as an apology.

If we find that we need to test the accuracy of your meter, we'll make an appointment with you. If we don't do this within 7 working days, we'll pay you &pound;22  if your concern relates to an electricity meter or &pound;20 for a gas meter

### Failure of a prepayment meter
If you report that there's a problem with your prepayment meter, for example if it doesn't accept credit, we'll send someone out to investigate.

If the problem is with an electricity prepayment meter, we'll send someone out  within 3 hours if you tell us between 7am and 7pm on a working day.  If you tell us between 9am and 5pm on a Saturday or Sunday or a bank holiday,   we'll be with you within 4 hours. If we don't do this, we'll pay you &pound;22.

If the problem is with a gas prepayment meter and you tell us between 8am and 8pm on a working day, or between 9am and 5pm on a Saturday or Sunday or a bank holiday,  we'll be with you within 4 hours. If we're late, we'll pay you &pound;20.

### When we make appointments
If we need to come to your home to carry out some work, we'll always arrange an appointment. If we don't turn up when we say we'll, we'll pay you &pound;22 if the work was to do with electricity and &pound;20 if the work was to do with gas.

### Paying you
If we do owe you any money as described above, we'll pay you in 10 working days. If we don't, we'll pay you a further &pound;22 if the money we owed you was related to electricity and &pound;20 if it was related to gas.

<br>
## Overall Standards
<br>
### Moving meters
If you want us to move your meter we'll give you an appointment within 15 working days of you accepting our quote.

Target: we aim to do this100% of the time.

### Problems with prepayment meters
If your gas or electricity meter shuts off and it's not due to credit running out, let us know and we'll be with you in 3 hours on a working day to investigate an electricity meter and 4 hours for a gas meter. On any other day we'll be with you within 4 hours for either meter.

Target: we aim to do this 98% of the time on a working day and 95% of the time on any other day.

### Changing meters
If you change your price plan and need a different meter we'll fit it within 10 working days.

Target: we aim to do this 100% of the time.

### Restoring supply
If we've had to disconnect your supply - for example, if you've not paid your bill  for some time - we'll reconnect you by the end of the working day after the problem has been resolved.

Target: we aim to do this 100% of the time

## Standards that you can expect from your<br>electricity network company
<br>
### If your main fuse fails
If your main fuse fails and leaves you without electricity, your network company will be with you in a set number of hours.

These are:

&bull; Weekdays: call between 7am and 7pm and they'll be with you within 3 hours<br>
&bull; Weekends and bank holidays: call between 9am and 5pm and they'll be there within 4 hours.

If they take any longer, they'll pay you &pound;22.

### If you have repeated power cuts
If you have power cuts of more than 3 hours on 4 or more occasions in any year starting in April, the network company will pay you &pound;54.  But, please note, you'll need to claim it from them by the end of June after the year in question.

When you claim you'll need to tell the network company the address that was affected and the dates when you suffered the power cuts. You won't be able to claim if you've already received compensation.

### Power Shortages
In some very rare circumstances the network company may need to disconnect your supply if there is not enough power to supply everybody. The network company will make these power cuts as short as they can and you shouldn't be without power for more than 24 hours. If your power is off for longer than that you can claim &pound;54 from them, but you'll need to do so within 3 months of your power being reconnected.

### Planned power cuts
If your network company should need to disconnect you, perhaps to carry out work on the electricity system, they'll give you at least 2 days' notice. If they don't you can claim &pound;22 from them but you'll need to do so within a month of the power cut.

### Other power cuts
If you have a power cut, tell your network company and they'll have your power back on within 18 hours. If they don't, you can claim &pound;54 from them within 3 months. On top of that they'll pay you an additional &pound;27 for every 12 hours that you have no power.

If the problem affects more than 5,000 buildings, the network company has 24 hours to restore your supply before you can claim the &pound;54 and &pound;27 for every 12 hours that you're off supply after the initial 24.

It might be that the power was cut off due to particularly bad weather. In this case there may be many people off supply but you may still be able to claim compensation if your power is not back on within 24 to 48 hours. Again you'll need to claim within 3 months and, if it's accepted, you'll get &pound;27 with another &pound;27 for every additional 12 hours that you're off supply. However, they'll not pay you more than &pound;216.

The network company will assess your claim depending upon how bad the weather was. If they have between 8 and 13 times the normal number of faults in a day (or at least 8 times the normal number if they were caused by lightning) they should have your power back on within 24 hours.

If they have at least 13 times the normal number of faults in a day they should have you back on with 48 hours. If the weather is so bad that at least 35% of customers are affected they'll reconnect you in a period laid down in regulations.

### Voltage
If you're having problems with the voltage of your electricity and you tell your network company about it, they'll either offer an explanation within 5 working days or offer to carry out an investigation within 7 working days. If they do neither they'll pay you &pound;22.

### Making and keeping appointments
If your network company needs to visit you, you'll be offered a morning, afternoon or 2 hour time slot appointment. If they don't turn up at the time they've agreed to, they'll pay you &pound;22.

On some occasions any compensation you are owed will be paid automatically. If payments are due to bad weather either we or your network company will pay you as soon as we can and if we don't you'll get an additional &pound;22.

## Standards that you can expect from your gas transporter
If you are worried that you may have a gas leak:

&bull;	Put out any naked flames including cigarettes
&bull;	Don't switch any electrical appliance on or off
&bull;	Open windows and doors to let fresh air in
&bull;	Turn off any gas appliance that you think might be the cause of the leak. If it is easy and safe to do so, turn off the gas supply at the meter
&bull;	Call the National Gas Emergency Service on 0800 111 999.

### If your gas is unexpectedly disconnected
If your gas supply fails and you hadn't been warned about it, your gas transporter will reconnect you within 24 hours. If they don't you'll get &pound;30 plus a further &pound;30 for every extra 24 hour period that you're off supply up to a maximum of &pound;1,000.

### If your gas is planned to be turned off
The gas transporter may need to turn off your gas - for example to work on its pipes. They'll let you know at least 5 days before the work and tell you when and why your gas will be interrupted. If they don't you can claim &pound;20 within 3 months.

Once the transporter has finished its work you'll be reconnected within 5 working days. If they don't they'll pay you &pound;50 and a further &pound;50 for each period of 5 working days that you are not reconnected.

### New or altered connections
If you need to know where the gas pipes are on your land (known as a land enquiry), the gas transporter will respond in 5 working days. If they don't they'll pay you &pound;40, with a further &pound;40 for each day that they don't respond.

If you ask for a standard new connection, or a straight-forward change to your existing one, the transporter will reply within 6 working days. If they don't respond they'll pay you &pound;10 with a further &pound;10 for every day after that they don't reply.

If the required work is a bit more complicated and it is to a small or medium connection (up to and including 275 kW per hour) the gas transporter has 11 days to reply before they have to pay the &pound;10. If the connection is greater than 275 kW per hour they have 21 days to respond and if you don't hear from them in that time they'll pay &pound;20, plus &pound;20 for every day after that until they send a quote.

If the quote is not accurate as described in the transporter's published accuracy scheme you can ask for a re-quote and be refunded any money you're overpaid. For more detail speak with your gas transporter.

Once you accept a quote from the transporter, they'll contact you in 20 working days to let you know when the work will take place. If they don't respond in that time they'll pay &pound;20 for small and medium supplies and &pound;40 for larger ones, plus the same again for each working day that they don't respond.

If the transporter doesn't complete the work in the time that they've agreed with you, they'll pay you compensation. How much depends upon how much the quote was for.

<table id="compensation"><tr><th>Quote</th><th>Compensation</th><th>Maximum</th></tr><tr><td>Up to and including &pound;1,000</td><td>&pound;20</td><td>Lesser of &pound;200 or the value of the quote</td></tr><tr><td>&pound;1,001 - &pound;4,000</td><td>Lesser of &pound;100 or 2.5% of the value of the quote</td><td>25% of the quote</td></tr><tr><td>&pound;4,001 - &pound;25,000</td><td>&pound;100</td><td>25% of the quote</td></tr><tr><td>&pound;20,001 - &pound;50,000</td><td>&pound;100</td><td>&pound;5,000</td></tr><tr><td>&pound;50,001 - &pound;100,000</td><td>&pound;150</td><td>&pound;9,000</td></tr></table>
### Loss of gas to disadvantaged customers

If you're a Priority Services Registered customer and you lose your gas supply, whether planned or not, inform your transporter and they'll provide alternative means to heat your home and cook. If you let them know between 8am and 8pm you'll get these within 4 hours (or 8 hours if it's a large problem affecting more than 250 people). If you don't get this you can claim &pound;24 provided you claim within 3 months.

<br>

----

Meta_description: 

----

Meta_description_description: 