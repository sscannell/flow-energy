<?php



if($_POST['email'] == $_POST['confirm']) {
     
    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "Alex.Heys@flowenergy.uk.com";
	//$email_to = "stecove@gmail.com";
    $email_subject = "Somebody has just filled out the Flow Energy Survey";
     
     
    function died($error) {
        header('Location: /the-boiler/errors');
        die();
    }
     
    // validation expected data exists
	
    if(!isset($_POST['title']) ||
	!isset($_POST['first_name']) ||
        !isset($_POST['last_name']) ||
        !isset($_POST['email']) ||
        !isset($_POST['telephone']) ||
        !isset($_POST['postcode']) ||
		!isset($_POST['q1']) ||
		!isset($_POST['q2']) ||
		!isset($_POST['q3'])
		) {
        died('We are sorry, but there appears to be a problem with the form you submitted, please ensure all fields are completed.');       
    }
	$title = $_POST['title']; // required
     
    $first_name = $_POST['first_name']; // required
    $last_name = $_POST['last_name']; // required
    $email_from = $_POST['email']; // required
    $telephone = $_POST['telephone']; // not required
    $postcode = $_POST['postcode']; // required
	$q1 = $_POST['q1']; // required
		$q2 = $_POST['q2']; // required
		$q3 = $_POST['q3']; // required
		$khw = $_POST['khw']; // required
		$pounds = $_POST['pounds']; // required
		
     
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }
    $string_exp = "/^[A-Za-z .'-]+$/";
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
  }
  if(!preg_match($string_exp,$last_name)) {
    $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
  }
  
  if(strlen($error_message) > 0) {
    died($error_message);
  }
    $email_message = "<h2>Form details below.<br><br></h2>";
     
	 //added line breaks to bad SC
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href","\n","\r","\r\n");
      return str_replace($bad,"",$string);
    }
	//save to csv file SC

	$csvline = clean_string($title) . ",";
    $csvline .= clean_string($first_name) . ",";
    $csvline .= clean_string($last_name) . ",";
    $csvline .= clean_string($email_from) . ",";
    $csvline .= clean_string($telephone) . ",";
    $csvline .= clean_string($postcode) . ",";
	$csvline .= clean_string($q1) . ",";
	$csvline .= clean_string($q2) . ",";
	$csvline .= clean_string($q3) . ",";
	$csvline .= clean_string($khw) . ",";
	$csvline .= clean_string($pounds)."\n";
	
	file_put_contents("survey.csv", $csvline, FILE_APPEND);
	
	
	
     $email_message .= "<b>Title:</b> ".clean_string($title)."<br>";
    $email_message .= "<b>First Name:</b> ".clean_string($first_name)."<br>";
    $email_message .= "<b>Last Name:</b> ".clean_string($last_name)."<br>";
    $email_message .= "<b>Email:</b> ".clean_string($email_from)."<br>";
    $email_message .= "<b>Telephone:</b> ".clean_string($telephone)."<br>";
    $email_message .= "<b>Postcode:</b> ".clean_string($postcode)."<br>";
	$email_message .= "<b>Do you have gas heating?: </b>".clean_string($q1)."<br>";
	$email_message .= "<b>What kind of boiler do you have?:</b> ".clean_string($q2)."<br>";
	$email_message .= "<b>How many bedrooms does your house have?:</b> ".clean_string($q3)."<br><br>";
	$email_message .= "<b>How much gas do you use every year? </b>" . "<br><br>" . "kWh:" .clean_string($khw) . "<br>" . "£'s:" . clean_string($pounds)."<br><br>";
    //add link to csv SC
	$email_message .= "A spreadsheet containing a record of all<br> survey responces can be downloaded <a href=\"http://www.flowenergy.uk.com/survey.csv\">here</a>";
     
// create email headers
$headers = 'From: '.$email_from."\r\n".
$headers = 'Content-Type: text/html; charset=utf-8';
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  


 
header('Location: /the-boiler/thankyou');
 

 

}


else { header('Location: /the-boiler/errors');}?>

