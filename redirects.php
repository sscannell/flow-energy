<?php

$urlToRedirect = $_SERVER['REQUEST_URI']; // this is the requested URL 

if (false !== strpos($urlToRedirect, 'jacobbaileytest')) {
    
    $siteURL = "flowenergy.uk.com";

} else {

	$siteURL = "www.flowenergy.uk.com";

}

$urlList = array (/*    'requested url'            =>    'redirect url',             */    
                        "http://www.flowenergy.uk.com/quote-form.php"  =>  "http://quote.flowenergy.uk.com/",
						"http://www.flowenergy.uk.com/meet-flow"  =>  "http://$siteURL/the-flow-boiler/meet-flow",
						"http://www.flowenergy.uk.com/submit-a-reading.php"  =>  "http://www.flowenergy.uk.com/meter-readings/submit-a-meter-reading",
						"http://www.flowenergy.uk.com/ways-to-pay.php"  =>  "http://$siteURL/help/frequently-asked-questions",
						"http://www.flowenergy.uk.com/understanding-your-bill.php"  =>  "http://$siteURL/help/frequently-asked-questions/understanding-your-statement",
						"http://www.flowenergy.uk.com/understanding-your-online-account.php"  =>  "http://www.flowenergy.uk.com/pdf/Flow-User-Manual.pdf",
						"http://www.flowenergy.uk.com/faq.php"  =>  "http://$siteURL/help/frequently-asked-questions",
						"http://www.flowenergy.uk.com/probate.php"  =>  "http://$siteURL/help/frequently-asked-questions/probate",
						"http://www.flowenergy.uk.com/moving-home.php"  =>  "http://$siteURL/help/frequently-asked-questions/moving-home",
						"http://www.flowenergy.uk.com/useful-info.php"  =>  "http://$siteURL/help/flow-library",
						"http://www.flowenergy.uk.com/emergencies.php"   =>  "http://$siteURL/help/emergency-contacts",
						"http://www.flowenergy.uk.com/safety-advice.php"  =>  "http://$siteURL/help/frequently-asked-questions/safety-advice",
						"http://www.flowenergy.uk.com/how-to-switch.php"  =>  "http://$siteURL/help/frequently-asked-questions",
						"http://www.flowenergy.uk.com/refer-a-friend.php"  =>  "http://www.flowenergy.uk.com",
						"http://www.flowenergy.uk.com/energy-saving-tips.php"  =>  "http://$siteURL/help/frequently-asked-questions/energy-saving-tips",
						"http://www.flowenergy.uk.com/careers.php"  =>  "http://$siteURL/help/careers",
						"http://www.flowenergy.uk.com/our-promise.php"  =>  "http://$siteURL/great-service/what-you-should-expect",
						"http://www.flowenergy.uk.com/media-centre.php"  =>  "http://$siteURL/help/media-centre",
						"http://www.flowenergy.uk.com/energetix-group.php"  =>  "http://www.flowgroup.uk.com",
						"http://www.flowenergy.uk.com/get-in-touch/ways-to-contact-us"  =>  "http://$siteURL/get-in-touch/contact-us",
						"http://www.flowenergy.uk.com/emergency-numbers.php"  =>  "http://$siteURL/help/emergency-contacts",
						"http://www.flowenergy.uk.com/register-for-interest.php"  =>  "http://$siteURL/the-flow-boiler/register-interest",
						"http://www.flowenergy.uk.com/make-a-complaint.php"  =>  "http://$siteURL/help/complaints-procedure",
						"http://www.flowenergy.uk.com/social-media.php"  =>  "http://www.flowenergy.uk.com"
                );

if(array_key_exists('$urlToRedirect', $urlList)) {
    headers("Location: {$urlList[$urlToRedirect]}");
} 



