	<?php snippet('header') ?>

    <div class="content full-new">
    
    <div class="doublepadb borderBottom">
        <div class="col2">
            <?php echo kirbytext($page->text()) ?>
            <p></p><br />
            <h2 class="speech">Meet Flow</h2>
            <?php echo kirbytext($page->meet_flow_sub()) ?>
        </div>
        
        <div class="col2 alignCenter">
            <img src="<?php echo $page->boiler(); ?>" />
        </div>
        <div class="clear"></div>
    </div>
    
             
     <div class="theBenefits doublepad borderBottom">
        <div class="doublemarb alignCenter">
            <h1>The Benefits</h1>
         </div>
        <div class="col3">
            <img src="<?php echo url('images/benefits-01.png');?>"/><br />
            	<div class="stdpad">
					<?php echo $page->benefits_01(); ?>
              </div>
        </div>
        <div class="col3">
            <img src="<?php echo url('images/benefits-02.png');?>"/><br />
            	<div class="stdpad">
					<?php echo $page->benefits_02(); ?>
              </div>
        </div>
        <div class="col3">
            <img src="<?php echo url('images/benefits-03.png');?>"/><br />
            	<div class="stdpad">
					<?php echo $page->benefits_03(); ?>
              </div>
        </div>
        <div class="clear"></div>
    </div>
    
    <div class="theDeal stdpad stdmart">
         	<div class="col2">
				<?php echo kirbytext($page->the_deal()) ?>
          </div>
          <div class="col2">
          		<div class="alignCenter">
                	<div class="stdpadt">
                    <img src="<?php echo url('images/pays-itself.gif"'); ?>" />
                  </div>
              </div>
          </div>
        <div class="clear"></div>
    </div>
    
    <div class="greyBox stdmart">
    	<div class="doublepad">
         <h2 class="blueTitle fl">Excited now?</h2>
            <div class="fr">
                <a class="button blue fl" 
                href="<?php echo url('the-flow-boiler/how-it-works');?>">
                See how the deal works</a>
                <a class="button black fl" 
                href="<?php echo url('the-flow-boiler/register-interest');?>">
                Register your interest</a>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
        
        <div class="clear"><!-- --></div>
    </div>
    
    
    <script src="http://a.vimeocdn.com/js/froogaloop2.min.js?1ec83-1364333740"></script>
    <script>
        /**
         * Called once a vimeo player is loaded and ready to receive
         * commands. You can add events and make api calls only after this
         * function has been called.
         *
         * @param String $player_id — id of the iframe element firing the event. This is
         * useful when listening to multiple videos so you can identify which one fired
         * the event.
         */
        function ready(player_id) {
            // Keep a reference to Froogaloop for this player
            var player = $f(player_id),

                playButton = document.getElementById('playButton'),
                pauseButton = document.getElementById('pauseButton'),
                unloadBUtton = document.getElementById('unloadButton');

            /**
             * Attach event listeners.
             *
             * If you're using a javascript framework like jQuery or Mootools
             * you'll probably use their addEvent method to add the click events.
             * Here we're just using the standard W3C addEventListener method. If
             * you need IE8 support, you'll need to use attachEvent for IE8 and
             * addEventListener for everything else (or just use jQuery or MooTools).
             */

            playButton.addEventListener('click', function() {
                player.api('play');
            });

            pauseButton.addEventListener('click', function() {
                player.api('pause');
            });

            unloadButton.addEventListener('click', function() {
                player.api('unload');
            });
        }

        window.addEventListener('load', function() {
            //Attach the ready event to the iframe
            $f(document.getElementById('player')).addEvent('ready', ready);
        });
    </script>
    
  
    
    
<?php snippet('footer') ?>