<?php snippet('header') ?>
<div class="content full-new">
    <h1>Sitemap</h1> 
    
    
    <?php foreach($pages->visible() as $page): ?>
    <?php if($page->title() != 'The Boiler'):?><ul class="sitemap"><?php endif;?>
    
        <li class="title <?php if($page->title() == 'The Boiler'):?>margin<?php endif;?>"><?php echo html($page->title()) ?></li>
        <?php $subpages = $page->children()->visible();?>
        <?php if($subpages && $subpages->count()):?>
            
            <?php foreach($subpages as $subpage):?>
            <li><a href="<?php echo ($subpage->redirect_url()) ? $subpage->redirect_url() : $subpage->url() ?>"><?php echo html($subpage->title()) ?></a></li>
            <?php endforeach;?>
            
        <?php endif;?>
    <?php if($page->title() != 'Quotes & Tariffs'):?></ul><?php endif;?>
    <?php endforeach;?>
    
    
    
         
</div>
<div class="clear"><!-- --></div>
<?php snippet('footer') ?>