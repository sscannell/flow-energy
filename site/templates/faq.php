<?php snippet('header') ?>
<div class="content full-new accordion">
    <?php if($site->uri()->path()->last() == 'submit-a-meter-reading'):?>
    <div class="img-right">
        <img src="<?php echo url("assets/images/flow-meter.png");?>" />
    </div>
    <?php endif;?>
    <?php echo kirbytext($page->text()) ?>

<?php /////// EXISTING CUSTOMERS ///////// ?>

     <p class="stdpadb">  
        <a id="link0" class="linkBar faq-button" href="<?php echo url("#");?>">Information for existing customers about the Thames Tariff<span class="openButton">Open</span></a>
            <div class="section0 acc-sec">
			


<h1>The Thames Tariff</h1>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">I'm currently on the Thames Tariff. What are my options?</a><br />
<em>We've extended the Thames Tariff so it will no longer expire on 31st August 2014. Instead, it will continue in two versions. The first version is a Fixed Rate version that expires on 31st August 2015. The second is a variable version of the Thames Tariff. You can choose whichever you like. Obviously, with the Fixed Rate version, your prices are fixed until 31st August 2015. With the variable version, we may have to vary the prices if wholesale energy prices rise. The choice is up to you.  But you don’t have to decide now.  We’ll write to you soon to explain your choices again and to ask you for your decision.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Is the Thames Tariff exactly the same?</a><br />
<em>Yes, it's exactly the same apart from one small thing: there's no exit fee on the Fixed Rate version after 31st August 2014. So, even if you fix until 31st August 2015, you won't have to pay any exit fees if you decide to leave.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">What happens if I don't do anything and my original Thames Tariff expires on 31st August 2014?</a><br />
<em>You'll simply stay on the variable Thames Tariff. We won't move you to a more expensive tariff (unlike other energy companies).</em></p>

<p class="doublemart">Didn't find your answer? <a href="<?php echo url('get-in-touch/contact-us'); ?>"><strong>Then please get in touch</strong></a></p>


</div></p>



<?php /////// SWITCHING TO FLOW ///////// ?>

     <p>  
        <a id="link1" class="linkBar faq-button" href="<?php echo url("#");?>">Information for customers who are switching to Flow<span class="openButton">Open</span></a>
            <div class="section1 acc-sec">
			


<h1>Switching to Flow</h1>



<h3>The switch</h3>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">How long will the switch take?</a><br />
<em>It normally takes 4 - 6 weeks.  Occasionally it takes longer than this  but, if it's going to, we'll let you know.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">One of my supplies has been switched to Flow but the other hasn&rsquo;t.  What&rsquo;s happening?</a><br />
<em>There are sometimes delays in transferring one of a customer’s fuels.  For example, in general, switching the gas supply is often slightly slower than switching electricity.  But there may be other issues that are caused by the industry’s systems for switching.  If you’ve received confirmation from us that one of your fuels has switched but you haven’t yet heard about the other, please don’t worry.  We will send you an email or letter as soon as we have confirmation that both switches will soon be complete.</em></p>

<h3>Direct Debits</h3>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">When will my first Direct Debit be taken?</a><br />
<em>It’ll be taken on the first day of supply.  Click <a href="http://www.flowenergy.uk.com/help/direct-debits">here</a> for more information about Direct Debits.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">My Direct Debit has been taken but it seems too low.  What&rsquo;s happening?</a><br />
<em>If there has been a delay in switching either your gas or electricity, then we will often take a reduced Direct Debit amount.  That’s because we don’t think it’s fair to take the whole amount when we aren’t supplying you with both gas and electricity yet.  As soon as both fuels are on supply we’ll start to take the whole Direct Debit amount.</em></p>

<h3>Meter readings</h3>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">When and how do I provide my first meter reading?</a><br />
<em>Please provide your first meter reading as near as possible to the supply start date we have given you.  So, for example, if we’ve told you that you’ll be coming on supply with Flow on the 1st September, please read your meter as close as possible to the 1st September.  To submit a meter reading, as well as to access a full guide on how to read your meter, please click <a href="http://www.flowenergy.uk.com/help/submit-a-meter-reading">here.</a></em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">I supplied a meter reading to you but my old supplier has used a different reading to close my account.  What should I do?</a><br />
<em>If you see that this has happened, let us know immediately.  We’ll then resubmit your meter reading to your old supplier.</em></p>

<h3>Welcome pack and communications</h3>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">I haven&rsquo;t received my welcome pack yet.</a><br />
<em>The welcome pack isn't really a pack.  It’s simply the final letter that we send by email, when your switch is complete, which gives a summary of all your account details.  Knowing this, if you still haven't received it, then please let us know.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">My switch is complete.  What communication should I expect from you?</a><br />
<em>We know that customers, in general, don’t want to hear from their energy supplier!  So, unless we need to tell you about something important, we’ll leave you alone.  Of course, if there’s ever anything you need to tell us, just get in touch.</em></p>

<h3>The Government's Warm Home Discount Scheme</h3>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Is Flow part of the Government's Warm Home Discount Scheme?</a><br />
<em>The simple answer is no.  Only larger suppliers have pockets deep enough to afford the scheme.</em></p>
		

<p class="doublemart">Didn't find your answer? <a href="<?php echo url('get-in-touch/contact-us'); ?>"><strong>Then please get in touch</strong></a></p>
        	
</div></p>


<?php /////// STATEMENTS ///////// ?>
        
      <p>  <a id="link2" class="linkBar faq-button" href="<?php echo url("#");?>">Statements, the online portal and your account number<span class="openButton">Open</span></a>
        	<div class="section2 acc-sec">
			

<h1>Statements, the online portal and your account number</h1>

<p>Click the button here to download the Flow portal user manual:</p> 
<p><a class="button blue fr" href="http://www.flowenergy.uk.com/pdf/Flow-User-Manual.pdf">Download User Manual</a> </p>
<div class="clear"></div>

<p>Or click the questions below to see the answers.</p>

<p class="stdmart"><a href="http://www.flowenergy.uk.com/#" class="faq-button">How often will I receive a statement?</a><br />
<em>If you've chosen to receive statements in the post then you’ll receive a statement every 6 months. If you don't receive paper statements then we'll generate a statement for you every month, at the beginning of the month. You'll then be able to view this using the online portal.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Can I receive statements in the post?</a><br />
<em>You can.  You’ll receive one every 6 months and it costs £3 per year per fuel.</em></p>


<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">I've received a statement and there's something I don't understand.</a><br />
<em>Click <a href="http://www.flowenergy.uk.com/help/frequently-asked-questions/understanding-your-statement">here</a> to view an explanation of the different parts of your statement.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Can I manage my account online?</a><br />
<em>Yes, you can.  Using the Flow portal you’ll be able to view monthly statements, submit meter readings and make payments, all online.  Before you use the Flow portal for the first time, you'll need to register.  To register, you'll just need to enter some of the account details we hold on record for you.  Click <a href="https://your.flowenergy.uk.com/">here</a> to access the portal.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">I'm having trouble registering to use the Flow portal, what can I do?</a><br />
<em>The information you need to provide when registering to use the Flow portal needs to exactly match the information we have on record for you.  So, you'll need to enter the same email address and the same phone number that we have on your account.  If you can't find your account number, please see the FAQ below.  And if you still can't register, then please do give the team a call.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Where can I find my account number?</a><br />
<em>Your account number can be found top right of any of the letters we have emailed to you. So, when you receive an email with an attachment from us, open up the attachment. Top right you'll see the big Flow logo and directly underneath will be the date and your account number. It starts with 21 and it's eight digits long.</em></p>
            
<p class="doublemart">Didn't find your answer? <a href="<?php echo url('get-in-touch/contact-us'); ?>"><strong>Then please get in touch</strong></a></p>

            
            </div></p>
  
  
 <?php /////// DIRECT DEBITS AND PAYMENTS ///////// ?> 
  
        
        <p><a id="link3" class="linkBar faq-button" href="<?php echo url("#");?>">Direct Debits and payments<span class="openButton">Open</span></a>
        	<div class="section3 acc-sec">
			
                    <h1>Direct Debits &amp; payments</h1>

<p>Please take a minute to see if your question is answered below. Click the question to see the answer. </p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">How does my monthly payment plan work?</a><br />
<em>Your monthly Direct Debit amount is the same every month. But, of course, you use different amounts of energy each month.  So, for example, in summer, your account may be in credit – because the amount you pay is more than the cost of the energy you use. This credit balance will then gradually be used up over winter, when you’re using more energy.  Please click <a href="http://www.flowenergy.uk.com/help/frequently-asked-questions/flow-energy-payment-plan">here</a> to watch a short video that explains everything you need to know.  If your account is persistently in credit or debit, then we may change your Direct Debit amount. We review Direct Debits every 6 months.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Can I make an additional payment to my account?</a><br />
<em>Yes, of course.   You can do this using the online portal.</em></p>

<p><br><br />
<a href="http://www.flowenergy.uk.com/#" class="faq-button">When will my first Direct Debit be taken?</a><br />
<em>It will be taken on your supply start date.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Can I change the date of my Direct Debit?</a><br />
<em>Your first Direct Debit has to be taken on your supply start date.  However, after that you can change the date on which it is taken.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Can I change the amount of my Direct Debit?</a><br />
<em>If you think that your Direct Debit is too high or too low then let us know and we can discuss changing it.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">The Direct Debit amount you&rsquo;ve taken is lower than you said it would be.</a><br />
<em>If there has been a delay in one of your fuels coming on supply then we will reduce the amount we take as a Direct Debit until both fuels come on supply.  Once both fuels are on supply, we’ll increase your Direct Debit back to the amount we originally quoted to you.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">How often do you review and potentially change Direct Debit amounts?</a><br />
<em>We review Direct Debits every 6 months, unless you ask us to review it sooner.  When we review your Direct Debit we’ll work out whether we need to change it or not.  Your account should be in credit at the beginning of winter.  This is so that you have enough credit in your account to cover increased usage over winter.  Equally, your account should be in debit at the end of winter.  Less usage over spring and summer will allow this debit balance to clear.  If your account is running differently to the above, then we may amend your Direct Debit amount to help you get into this position.</em></p>


<p class="doublemart">Didn't find your answer? <a href="<?php echo url('get-in-touch/contact-us'); ?>"><strong>Then please get in touch</strong></a></p>
            
            </div></p>


<?php /////// METERS & METER READINGS ///////// ?>

            
        <p><a id="link4" class="linkBar faq-button" href="<?php echo url("#");?>">Meters & meter readings<span class="openButton">Open</span></a>
        	<div class="section4 acc-sec">
			        <h1>Meter readings</h1>



<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">How do I provide a meter reading?</a><br />
<em>You can now submit a meter reading using the Flow online portal. Before you use the Flow portal for the first time, you'll need to register.  To register, you'll just need to enter some of the account details we hold on record for you.  Click <a href="https://your.flowenergy.uk.com/">here</a> to access the portal.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">When do I provide my first meter reading?</a><br />
<em>Please provide your first meter reading as near as possible to the supply start date we have given you.  So, for example, if we’ve told you that you’ll be coming on supply with Flow on the 1st September, please read your meter as close as possible to the 1st September.  Click <a href="http://www.flowenergy.uk.com/your-account/submit-a-meter-reading">here</a> for more information about meter readings.</em></p>

<!--<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">The meter readings I submitted don't appear on my statement or in the portal.  Why not?</a><br />
<em>Meter readings that you supply are used to generate accurate estimates at the start and the end of the month.  However, PLEASE NOTE that meter readings you supply will not appear on your statement or in the portal.  Please click <a href="http://www.flowenergy.uk.com/your-account/your-meter-readings">here</a> for a full explanation of this.</em></p>-->

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">I tried to submit a meter reading in the portal, but it said my meters weren't registered. What's happening?</a><br />
<em>When you switch to Flow, part of the process is an agency linked to your previous supplier sending us the technical details for your meter. Sometimes, there's a delay in us receiving these details.  Without them, we can't accept meter readings from you.  Don't worry if this is the case with your account. We know we're still waiting for these details and we are chasing.  Your account will be updated as soon as possible with your meter technical details and you'll then be able to supply readings online.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">How often should I provide meter readings once I&rsquo;ve switched to Flow?</a><br />
<em>Ideally, you should provide meter readings every month.  We’ll use these readings to work out how much energy you've used every month.  If you don’t want to provide readings every month then that’s absolutely fine although you should aim to read your meter(s) at least once every six months.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">I supplied a meter reading to you but my old supplier has used a different reading to close my account.  What should I do?</a><br />
<em>If you see that this has happened, let us know immediately.  If there's a significant difference, then we'll be able to dispute the read with your old supplier.</em></p>

<p><br></p>

<p class="doublemart">Didn't find your answer? <a href="<?php echo url('get-in-touch/contact-us'); ?>"><strong>Then please get in touch</strong></a></p>

            </div></p>
 
 
 
 <?php /////// SMART METERS ///////// ?>
        
        <p><a id="link5" class="linkBar faq-button" href="<?php echo url("#");?>">Smart meters<span class="openButton">Open</span></a>
        	<div class="section5  acc-sec">
			        <h1>Smart meters</h1>



<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Why can&rsquo;t I use my meter&rsquo;s Smart functionality with Flow?</a><br />
<em>We currently don't support all your Smart meter's functions as we don't have the infrastructure and agreements in place.  However, we will in the future.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Will you be offering Smart meters in the future?</a><br />
<em>Yes we will.  We don’t currently support Smart metering as we’re a new, small supplier.  But, in the future, we will be providing Smart metering and Smart meters and we’ll let you know as soon as we are.</em></p>

<p class="doublemart">Didn't find your answer? <a href="<?php echo url('get-in-touch/contact-us'); ?>"><strong>Then please get in touch</strong></a></p>

            </div></p>


<?php /////// THE FLOW BOILER ///////// ?>
        
        <p><a id="link6" class="linkBar faq-button" href="<?php echo url("#");?>">The Flow boiler<span class="openButton">Open</span></a>  
        	<div class="section6 acc-sec">
			
                    <h1>The Flow boiler</h1>

<p>The answers to many of the most frequently asked questions about the Flow boiler are below.  Once you've found what you're looking for, <a href="http://www.flowenergy.uk.com/get-in-touch/register-interest">register your interest</a> and we'll contact you first when the Flow boiler launches later this year. </p>

<p>&nbsp;<br><br></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">When will the Flow boiler be available?</a><br />
<em>We're excited to say that the Flow boiler will launch in the second half of 2014.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Can I be part of the Flow boiler pilot?</a><br />
<em>We've recruited all our candidates for the Flow boiler pilot now so you can't apply - sorry about that.  But it's not too long to wait for the full launch.<a href="<?php echo url ('get-in-touch/register-interest'); ?>">Register your interest</a>and we'll contact you first when the Flow boiler launches later this year.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Is the Flow boiler suitable for all houses?</a><br />
<em>The Flow boiler is an 18kw condensing boiler that requires a separate hot water tank.  You'll need to be using roughly between 28,000 and 52,000 kWh of gas a year for the Flow boiler to be suitable.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">How does the Flow boiler pay for itself?</a><br />
<em>It works like this: we provide you with a Flow boiler and you only have to pay for installation.  Then you switch your home energy to Flow on a competitive tariff for five years.  During those five years, you pay for all the gas and electricity you use as normal.  But some of the electricity you use is actually being generated by the Flow boiler, meaning that we don't have to buy that electricity.  The money this saves us pays off the cost of the boiler - so you don't have to.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">How much electricity does the Flow boiler generate?</a><br />
<em>The Flow boiler generates about 2000kWh of electricity a year when the household is using roughly 35,000kWh of gas.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Will the Flow boiler reduce my bills?</a><br />
<em>For the first five years, the electricity the boiler generates pays off the cost of the boiler.  After those first five years the electricity the boiler generates will reduce your electricity bills.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Will the Flow boiler reduce my household carbon emissions?</a><br />
<em>Yes, absolutely, from the word go.  If you're replacing an older boiler, then the Flow boiler will automatically reduce your carbon emissions because it will be more efficient.  However, the Flow boiler also reduces your household carbon emissions by generating low-carbon electricity.  This electricity is generated much more efficiently than electricity from big power stations and none is lost in transmission down power lines because it's generated and used in the home. So, when the Flow boiler produces 2000kWh of electricity, it reduces your household carbon emissions by about 1000kg.  That's roughly the same as driving 3000 miles less in the average UK car.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">How efficient is the Flow boiler?</a><br />
<em>The Flow boiler is an A-rated appliance with a 92% efficiency.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">How big is the Flow boiler and how much does it weigh?</a><br />
<em>The dimensions of the boiler are 495mm width X 425mm depth X 850mm height. It weighs about 95kg. So, it’s a similar size to other domestic models.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">What does microCHP mean?</a><br />
<em>The CHP in microCHP stands for Combined Heat and Power.  It means that the Flow boiler generates electricity at the same time as it's providing heat for your home.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">Does microCHP technology attract the Feed-In Tariff?</a><br />
<em>Yes it does, for the first 30,000 UK domestic microCHP customers. However, if we have provided the boiler to you and you have only paid for installation, then the Feed-In Tariff will go towards paying for the cost of the boiler.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">How does the Feed-In Tariff work if I also have solar panels?</a><br />
<em>You would still get your Feed-In Tariff for your solar panels and the Flow boiler would attract the Feed-In Tariff too (although please also note the answer to the question above).</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">What's the power consumption of the boiler?</a><br />
<em>On standby it’s about 10 watts. Generating, it’s about 200 watts maximum, with losses dependent on power output. Fuel consumption at the maximum burner firing rate is 1.8m3 per hour.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">What's the output of the boiler?</a><br />
<em>The electrical output is up to 1000 watts. Thermal output is: Minimum 7.2 kilowatts condensing, 6.8 kilowatts non-condensing. Maximum 18.0kw condensing, 16.9 kw non-condensing.</em></p>

<p><a href="http://www.flowenergy.uk.com/#" class="faq-button">How does the boiler connect to the network in the home and to the National Grid?</a><br />
<em>The boiler will connect to the low voltage (230V) mains network in a customer’s house. The appliance incorporates an electrical power controller which monitors the mains and conditions the generated electricity to inject into the household ring main. In most cases it is expected that the electrical power will be consumed in the home. If the home's demand is lower than the electricity generated, then the excess is exported through the household meter into the local network.</em></p>
    

<p class="doublemart">Didn't find your answer? <a href="<?php echo url('get-in-touch/contact-us'); ?>"><strong>Then please get in touch</strong></a></p>
            
            </div></p>
            
</div>
<div class="clear"><!-- --></div>
<?php snippet('footer') ?>