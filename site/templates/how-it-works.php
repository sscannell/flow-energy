	<?php snippet('header') ?>

    <div class="content full-new">
    
		<?php echo kirbytext($page->text()) ?>
        
        <div class="doublemart alignCenter">
            <img src="<?php echo url('images/works-speech-bubble.png');?>"/>
        </div>
        
    	<ul class="dealList stdmart">
        	<li class="first">
            	<div class="doublepad">
            	  <div class="col20 alignCenter">
                      <div class="listCircle">1</div>
                 </div>
                 <div class="col40">
						<?php echo kirbytext($page->stage_1()) ?>
                </div>
                 <div class="col40 alignCenter">
                    <img src="<?php echo url('images/works-flow-boiler.png');?>"/>
                </div>
                <div class="clear"></div>
               </div>
            </li>
           <li>
           	<div class="doublepad">
                <div class="col20 alignCenter">
                          <div class="listCircle">2</div>
                   </div>
                   <div class="col40 alignCenter">
                        <img src="<?php echo url('images/works-flow-logo.png');?>"/>
                   </div>
                   <div class="col40">
						<?php echo kirbytext($page->stage_2()) ?>
                   </div>
                   <div class="clear"></div>
               </div>
           </li>
           <li>
           	<div class="doublepad">
                <div class="col20 alignCenter">
                      <div class="listCircle">3</div>
                  </div>
                  <div class="col40">
						<?php echo kirbytext($page->stage_3()) ?>
                   </div>
                  <div class="col40 alignCenter">
                        <img src="<?php echo url('images/works-nothingtopay.png');?>"/>
                   </div>
                   <div class="clear"></div>
               </div>
           </li>
           <li>
           	<div class="doublepad">
                <div class="col20 alignCenter">
                          <div class="listCircle">4</div>
                   </div>
                   <div class="col40 alignCenter">
                        <img src="<?php echo url('images/works-piggybank.png');?>"/>
                   </div>
                   <div class="col40">
						<?php echo kirbytext($page->stage_4()) ?>
                   </div>
                   <div class="clear"></div>
               </div>
            </li>
           <li>
           	<div class="doublepad">
                <div class="col20 alignCenter">
                          <div class="listCircle">5</div>
                   </div>
                   <div class="col40">
						<?php echo kirbytext($page->stage_5()) ?>
                   </div>
                   <div class="col40 alignCenter">
                        <img src="<?php echo url('images/works-carbon-footprint.png');?>"/>
                   </div>
                   <div class="clear"></div>
               </div>
           </li>
        </ul>
        
        <div class="doublemart textRight">
            <h3>The Flow boiler is the future of home energy.<br />Register your interest today.</h3><br />
            	<div class="fr stdmart">
                <a class="button blue fl stdmarr" 
                href="<?php echo url('the-flow-boiler/flow-boiler');?>">
                FAQs</a>
                <a class="button black fl noMargin" 
                href="<?php echo url('the-flow-boiler/register-interest');?>">
                Register your interest</a>
                <div class="clear"></div>
              </div>
              <div class="clear"></div>
        </div>
        
        <div class="video">
            <iframe id="player" src="http://player.vimeo.com/video/62717175?api=1&amp;player_id=player&color=67c7e3" width="919" height="517" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
<!-- <img src="http://www.flowenergy.uk.com/images/video-screen.jpg" width="578" height="325" alt="Video Screen"> -->
        </div>             
        	
    </div>
    
    
    
  
    
    
<?php snippet('footer') ?>