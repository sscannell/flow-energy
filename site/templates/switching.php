<?php snippet('header') ?>
<div class="content full-new">
    <?php if($site->uri()->path()->last() == 'submit-a-meter-reading'):?>
    <div class="img-right">
        <img src="<?php echo url("assets/images/flow-meter.png");?>" />
    </div>
    <?php endif;?>
    
    <h1>Email us</h1>
    <h2>Please choose what you'd like to talk to us about from the list below.</h2>
    
    <p><a class="linkBar" href="<?php echo url("get-in-touch/contact-us/");?>">Information for customers who are switching to Flow<span class="closeButton">Close</span></a></p>

    
    <div class="doublemarv">
		<?php echo kirbytext($page->text()) ?>
    </div>
    
     <p>  
        <a class="linkBar" href="<?php echo url("get-in-touch/account-number/");?>">Statements, the online portal and your account number<span class="openButton">Open</span></a>
        <a class="linkBar" href="<?php echo url("get-in-touch/direct-debits/");?>">Direct Debits and payments<span class="openButton">Open</span></a>
        <a class="linkBar" href="<?php echo url("get-in-touch/meter-readings/");?>">Meters & meter readings<span class="openButton">Open</span></a>
        <a class="linkBar" href="<?php echo url("get-in-touch/smart-meters/");?>">Smart meters<span class="openButton">Open</span></a>
        <a class="linkBar" href="<?php echo url("get-in-touch/flow-boiler/");?>">The Flow boiler<span class="openButton">Open</span></a>  
    </p> 
</div>
<div class="clear"><!-- --></div>
<?php snippet('footer') ?>