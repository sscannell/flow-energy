<?php snippet('header') ?>
    <div class="content full-new useful-info">
        <?php echo kirbytext($page->text()) ?>
        <div class="clear"><!-- --></div>
    </div>
    <div class="grey-container">
        <div class="grey-container-content">
        <?php echo kirbytext($page->grey_text()) ?>
        <div class="clear"><!-- --></div>
        </div>
    </div>
    
    
<?php snippet('footer') ?>