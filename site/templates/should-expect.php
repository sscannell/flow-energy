	<?php snippet('header') ?>

<div class="content full-new">
    
    <div class="doublepadb">
            <?php echo kirbytext($page->text()) ?>
    </div>
        
    <div id="shouldExpect">
        <div class="col2">
            <div class="stdpad">
                <img class="doublepadt" 
                src="<?php echo url ('images/speech-bubble-graphic.png"'); ?>" />
           </div>
        </div>
        <div class="col2">
            <div class="stdpad">
                <?php echo kirbytext ($page->intro()); ?>
            </div>
        </div>
    
        <div class="clear"></div>
    
        <div class="stdmart">
            <div class="col2">
                <div class="stdpad">
                    <p class="quote">
							"<?php echo ($page->quote()); ?>"
                    <p><?php echo ($page->quote_author()); ?></p><br />
                    <a class="button blue" href="http://www.flowenergy.uk.com/great-service/what-our-customers-say">What our customers say</a>
                    </p>
                </div>
            </div>
            <div class="col2">
                <div class="stdpad">
                    <div class="blueBlockListWrap stdpad">
                        <h3>What you should expect</h3>
                        <ul class="blueBlockList">
                        	   <li><?php echo ($page->list1()); ?></li>
                        	   <li><?php echo ($page->list2()); ?></li>
                        	   <li><?php echo ($page->list3()); ?></li>
                        	   <li><?php echo ($page->list4()); ?></li>
                        	   <li><?php echo ($page->list5()); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"><!-- --></div>
</div>
    
    
    <script src="http://a.vimeocdn.com/js/froogaloop2.min.js?1ec83-1364333740"></script>
    <script>
        /**
         * Called once a vimeo player is loaded and ready to receive
         * commands. You can add events and make api calls only after this
         * function has been called.
         *
         * @param String $player_id — id of the iframe element firing the event. This is
         * useful when listening to multiple videos so you can identify which one fired
         * the event.
         */
        function ready(player_id) {
            // Keep a reference to Froogaloop for this player
            var player = $f(player_id),

                playButton = document.getElementById('playButton'),
                pauseButton = document.getElementById('pauseButton'),
                unloadBUtton = document.getElementById('unloadButton');

            /**
             * Attach event listeners.
             *
             * If you're using a javascript framework like jQuery or Mootools
             * you'll probably use their addEvent method to add the click events.
             * Here we're just using the standard W3C addEventListener method. If
             * you need IE8 support, you'll need to use attachEvent for IE8 and
             * addEventListener for everything else (or just use jQuery or MooTools).
             */

            playButton.addEventListener('click', function() {
                player.api('play');
            });

            pauseButton.addEventListener('click', function() {
                player.api('pause');
            });

            unloadButton.addEventListener('click', function() {
                player.api('unload');
            });
        }

        window.addEventListener('load', function() {
            //Attach the ready event to the iframe
            $f(document.getElementById('player')).addEvent('ready', ready);
        });
    </script>
    
  
    
    
<?php snippet('footer') ?>