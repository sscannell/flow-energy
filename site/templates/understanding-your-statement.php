<?php snippet('header') ?>
<div class="content full-new">
    <?php echo kirbytext($page->text()) ?>
    <div class="line"><!-- --></div>
    <div class="statement">
        <div class="img-left">
            <a href="<?php echo $page->statement_img_1() ?>" class="fancybox"><img src="<?php echo $page->statement_img_1() ?>" /></a>
        </div><br /><br /><br /><br /><br /><br /><br /><br />
        <?php echo kirbytext($page->statement_1()) ?>
        
        <div class="clear"><!-- --></div>
        <div class="line"><!-- --></div>
        
        <div class="img-left">
        <a href="<?php echo $page->statement_img_2() ?>" class="fancybox"><img src="<?php echo $page->statement_img_2() ?>" /></a>
        </div>
        <?php echo kirbytext($page->statement_2()) ?>
        <div class="clear"><!-- --></div>
        <div class="line"><!-- --></div>
        
        <div class="img-left">
        <a href="<?php echo $page->statement_img_3() ?>" class="fancybox"><img src="<?php echo $page->statement_img_3() ?>"/></a>
        </div>
        <?php echo kirbytext($page->statement_3()) ?>
        
        <div class="clear"><!-- --></div>
        <div class="line"><!-- --></div>
    </div>
    
</div>
<div class="clear"><!-- --></div>
<?php snippet('footer') ?>