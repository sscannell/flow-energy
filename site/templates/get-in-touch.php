	<?php snippet('header') ?>

    <div class="content full-new">
			<?php echo kirbytext($page->text()) ?>
        
        <div class="col3">
        	<div class="stdpadr stdpadt">
            <div class="borderBottom stdpadb">
				<?php echo kirbytext($page->col_1_title()) ?>
            </div>
            <br />
				<?php echo kirbytext($page->call_us_details()) ?><br /><br />
             <div class="greyBox">
             	<div class="stdpad">
				<?php echo kirbytext($page->opening_times()) ?>
             	</div>
             </div>
			</div>
        </div>
        
        <div class="col3">
        	<div class="stdpad">
            <div class="borderBottom stdpadb">
				<?php echo kirbytext($page->col_2_title()) ?>
            </div>
            <br />
				<?php echo kirbytext($page->faq_details()) ?>
            <br /><br /><br />
            <a href="<?php echo url('help/frequently-asked-questions');?>" class="button blue">
            View our FAQs</a>
          </div>
        </div>
                
        <div class="col3">
        	<div class="stdpad">
            <div class="borderBottom stdpadb">
				<?php echo kirbytext($page->col_3_title()) ?>
            </div>
            <br />
				<?php echo kirbytext($page->email_details()) ?>
            <br /><br /><br />
            <a href="<?php echo url('help/email-us');?>" class="button blue">
            Email us now</a>
          </div>
        </div>
        
        <div class="clear"></div>
                        	
    </div>
    
    
    
  
    
    
<?php snippet('footer') ?>