<?php snippet('header') ?>
    <script>
    $(window).load(function() {
        $('.flexslider').flexslider({
              controlNav : false,
              slideshow : true,
			   slideshowSpeed : 10000
        });
      });
    </script>
	
    <div class="flexslider-container">
    <div class="flexslider">
        <ul class="slides">
			<li>
            	<div class="col2">
                <div class="slide-holder">
                    <div class="slide-text">
                        <h2>Meet Flow</h2>
                    </div>
                    <div class="slider-content">
                        <h2>The revolutionary gas boiler that generates electricity 
                        as it heats your home. So clever it pays for itself.</h2>
                        <div class="clear"></div>
                        <a class="button fl black stdmart" href="<?php echo url('the-flow-boiler/meet-flow');?>">Meet Flow</a>
                    </div>

                </div>
               </div>
                   
              <div class="col2"> 
                <a class='vimeo' href="http://player.vimeo.com/video/62717175?api=1&amp;player_id=player&color=67c7e3">
                    <img src="<?php echo url('images/play-meet-flow.png'); ?>" /></a>                
               </div>
              <div class="clear"></div>
            </li>
			
			<li>
            	<div class="col2">
                <div class="slide-holder">
                    <div class="slide-text">
                        <h2>Get a quote</h2>
                    </div>
                    <div class="slider-content">
                        <h2>The best deal on home energy from a company you can trust.</h2>
                        <a class="button fl black stdmart" target="_blank" href="http://quote.flowenergy.uk.com/">Get a quote</a>
                    </div>

                </div>
               </div>
               <div class="col2">
                    <div class="slide-img">
                        <img src="<?php echo url('images/fam-animated.gif');?>" />
                    </div>
              </div>
              <div class="clear"></div>
            </li>
            
            
            
        </ul>
    </div>
    </div>
    <div id="home-grey-container">
        <ul id="home-boxes">
            <li class="first">
                <img src="<?php echo url('images/homepage-world.png');?>" />
                <h1><?php echo html($page->box_1_title()) ?></h1>
                <p><?php echo html($page->box_1_description()) ?></p>
                <a class="button grey" href="<?php echo html($page->box_1_button_link()) ?>"><?php echo html($page->box_1_button_text()) ?></a>
            </li>
            <li class="second">
                <img src="<?php echo url('images/homepage-care.png');?>" />
                <h1><?php echo html($page->box_2_title()) ?></h1>
                <p><?php echo html($page->box_2_description()) ?></p>
                <a class="button grey" href="<?php echo html($page->box_2_button_link()) ?>"><?php echo html($page->box_2_button_text()) ?></a>
            </li>
            <div class="clear"><!-- --></div>
        </ul>
    </div>
<?php snippet('footer') ?>