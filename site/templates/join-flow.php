<?php snippet('header') ?>
<div class="content full-new">
    <div class="inlineheading"><h1><?php echo ($page->call());?></h1></div><div class="inlinebutton"><a class="button blue" href="https://quote.flowenergy.uk.com">Get a quote now</a></div>
    <div class="clear"><!-- --></div>
</div>
<div class="grey-container">
    <div class="grey-container-content join-flow">
        <?php echo kirbytext($page->tagline());?>
  <div class="far-right"><img src="<?php echo url('images/flow-piggy-white.png');?>" alt="Flow Piggy"></div>
        <div class="right">
            <form class="join-flow">
                <label class="first"><?php echo ($page->item_1());?></label>
                <label class="second"><?php echo ($page->item_2());?></label>
                <label class="third"><?php echo ($page->item_3());?></label>
                <div class="clear"><!-- --></div>
                <a class="button blue" href="https://quote.flowenergy.uk.com">Get a quote now</a>
            </form>
        </div>
        <div class="clear"><!-- --></div>
    </div>
</div>
<div class="content full-new joinflow">
    <?php echo kirbytext($page->text()) ?>
    

    <a class="button blue fl" href="<?php echo url("quotes-tariffs/our-gas-and-electricity-tariff");?>">View our tariff</a><p class="fl">or</p><a class="button blue fl" href="https://quote.flowenergy.uk.com">Get an energy quote now</a>
    <div class="clear"><!-- --></div>

    
</div>
<div class="clear"><!-- --></div>
<?php snippet('footer') ?>