<?php global $post; include('emails/emailFunctions.php');?>
<?php snippet('header') ?>
<div class="content full-new">
    <?php echo kirbytext($page->text()) ?>
    
    <div id="form-start" class="clear"><!-- --></div>
        <?php if(!empty($_SESSION['success']) && $_SESSION['success'] == true): $_SESSION['success'] = false; ?>
            <div class="right first">
                <div class="line"><!-- --></div>
                <p class="email-success">Thanks! The emails have been sent.  If any of your friends sign up, we'll let you know by email and we'll send you your chosen reward in line with the terms and conditions. </p>
            </div>
        <?php else:?>
        <?php errorsBox();?>
        <form id="quote-form" class="share" action="<?php echo url("emails/refer-a-friend.php");?>" method="post">
            <div class="form-line">
                <div class="left">
                    <label>Choose your reward</label>                 
                </div>
                <div class="right">
                    <label class="big"><input type="radio" name="supplier" value="cash" class="styled" <?php echo (!empty($post['supplier']) && $post['supplier'] == 'cash') ? 'checked="checked"' : '';?> />cash</label>
                    <label class="big"><input type="radio" name="supplier" value="voucher" class="styled" <?php echo (!empty($post['supplier']) && $post['supplier'] == 'voucher') ? 'checked="checked"' : '';?> />vouchers</label>
                    <div class="select-box float">
                        <select name="voucher" class="small">   
                            <option value="">Select voucher</option>
                            <option <?php echo (!empty($post['voucher']) && $post['voucher'] == 'Amazon') ? 'selected="selected"' : '';?> value="Amazon">Amazon</option>
                            <option <?php echo (!empty($post['voucher']) && $post['voucher'] == 'M&S') ? 'selected="selected"' : '';?> value="M&amp;S">M&amp;S</option>
                            <option <?php echo (!empty($post['voucher']) && $post['voucher'] == 'iTunes') ? 'selected="selected"' : '';?> value="iTunes">iTunes</option>
                            <option <?php echo (!empty($post['voucher']) && $post['voucher'] == 'B&Q') ? 'selected="selected"' : '';?> value="B&amp;Q">B&amp;Q</option>
                            <option <?php echo (!empty($post['voucher']) && $post['voucher'] == 'next') ? 'selected="selected"' : '';?> value="next">next</option>
                        </select>
                    </div>
                </div>
                <div class="clear"><!-- --></div>
            </div>
            <div class="form-line">
                <div class="left">
                    <label>Enter your details</label>                 
                </div>
                <div class="right">
                    <input <?php if(!empty($errors)) findError('first' , $errors );?> type="text" placeholder="First name" name="first" value="<?php echo (!empty($post['first'])) ? $post['first'] : '';?>" />
                    <input <?php if(!empty($errors)) findError('last' , $errors );?> type="text" placeholder="Last name" name="last" value="<?php echo (!empty($post['last'])) ? $post['last'] : '';?>" />
                    <input <?php if(!empty($errors)) findError('email' , $errors );?> type="text" placeholder="Your email address" name="email" value="<?php echo (!empty($post['email'])) ? $post['email'] : '';?>" />
                </div>
                <div class="clear"><!-- --></div>
            </div>
            <div class="form-line">
                <div class="left">
                    <label>Enter your Flow account number</label>                 
                </div>
                <div class="right">
                    <input type="text" <?php if(!empty($errors)) findError('account' , $errors );?> placeholder="Your account number" name="account" value="<?php echo (!empty($post['account'])) ? $post['account'] : '';?>" />
                </div>
                <div class="clear"><!-- --></div>
            </div>
            <div class="form-line">
                <div class="left">
                    <label>Enter your friends' details</label>       
                </div>
                <div class="right">
                    <?php for($i = 1; $i<=6; $i++):?>
                    <input class="small-friend" type="text" <?php if(!empty($errors)) findError('friend_name_' .$i , $errors );?> placeholder="First name" name="friend_name_<?php echo $i;?>" value="<?php echo (!empty($post['friend_name_' . $i])) ? $post['friend_name_' . $i] : '';?>" />
                    
                    <input class="small-friend" type="text" <?php if(!empty($errors)) findError('friend_last_name_' .$i , $errors );?> placeholder="Last name" name="friend_last_name_<?php echo $i;?>" value="<?php echo (!empty($post['friend_last_name_' . $i])) ? $post['friend_last_name_' . $i] : '';?>" />
                    
                    <input class="small-friend" type="text" <?php if(!empty($errors)) findError('friend_email_' .$i , $errors );?> placeholder="Email address" name="friend_email_<?php echo $i;?>" value="<?php echo (!empty($post['friend_email_' . $i])) ? $post['friend_email_' . $i] : '';?>" /><br />
                    <?php endfor;?>
                    
                </div>
                <div class="clear"><!-- --></div>
            </div>
          
            <div class="form-line">
                <div class="left">
                    <!-- -->      
                </div>
                <div class="right">
                    <input type="submit" class="submit" value="Send emails now" />
                    <small><a href="<?php echo url('your-account/terms-refer-a-friend');?>" target="_blank">You'll need to be a Flow Energy customer to refer a friend. See the full terms and conditions here.</a></small>
                </div>
                <div class="clear"><!-- --></div>
            </div>
        </form>
        <?php endif;?>
        <div class="clear"><!-- --></div>
    
</div>
<div class="clear"><!-- --></div>
<?php snippet('footer') ?>