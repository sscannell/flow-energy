<?php snippet('header') ?>
    <div class="content full-new useful-info">
    	 <div class="col2">
				<?php echo kirbytext($page->text()) ?>
        </div>
    	 <div class="col2">
         
         	<div class="alignRight">
            <img src="<?php echo url('images/tariff-coming-soon.png');?>"/><br /><br />
            Enter your details and we’ll email you as soon as the Thames Tariff launches again.
         
            <form action="<?php echo $_SERVER['HTTP_HOST']; ?>/emails/lead-capture.php" method="post" id="watch-space">
                <input type="text" name="firstname" placeholder="First Name"><br/>
                <input type="text" name="lastname" placeholder="Last Name"><br/>
                <input type="text" name="email" placeholder="Email Address"><br/>
                <input type="text" name="confirmemail" placeholder="Confirm Email Address"><br/>
                <input type="submit" class="submit" value="Submit">
                
            </form>
            
            <script type="text/javascript">
                $(function(){
                    $('#watch-space').submit(function(ev){
                        ev.preventDefault();
            
                        $.post( "<?php echo $_SERVER['HTTP_HOST']; ?>/emails/lead-capture.php",$('#watch-space').serialize(), function( data ) {
                            $('p.message').remove();
                            $('#watch-space').append('<p class="message ' + data.type + '">' + data.message + '<p>');
                        });
                    })
                })
            </script>        
		</div>
     </div>
        <div class="clear"><!-- --></div>
    </div>
    <!--<div class="grey-container">
        <div class="grey-container-content">
        <?php echo kirbytext($page->grey_text()) ?>
        <div class="clear"></div>
        </div>-->
        
    
<?php snippet('footer') ?>