<?php snippet('header') ?>
<div class="content full-new">
    <?php echo kirbytext($page->text()) ?>
</div>
    <div class="grey-container">
        <div class="grey-container-content meet-flow">
            <div class="left">
                <?php echo kirbytext($page->grey_text()) ?>    
            </div>
            <div class="right">
                <div class="video">
                    <iframe id="player" src="<?php echo ($page->video()) ?>" width="578" height="325" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
<!-- <img src="http://www.flowenergy.uk.com/images/video-screen.jpg" width="578" height="325" alt="Video Screen"> -->

                </div>
            </div>
				<div class="clear"><!-- --></div>
				<div class="huh"><!-- --></div>
                <div class="inline">
                
                </div>
            <div class="clear"><!-- --></div>
            
            <div class="clear"><!-- --></div>
        </div>
    </div>
        <div class="content full-new">
        <?php echo kirbytext($page->bottom_text()) ?> 
        
        <div class="clear"><!-- --></div>
    </div>
    <script src="http://a.vimeocdn.com/js/froogaloop2.min.js?1ec83-1364333740"></script>
    <script>
        /**
         * Called once a vimeo player is loaded and ready to receive
         * commands. You can add events and make api calls only after this
         * function has been called.
         *
         * @param String $player_id — id of the iframe element firing the event. This is
         * useful when listening to multiple videos so you can identify which one fired
         * the event.
         */
        function ready(player_id) {
            // Keep a reference to Froogaloop for this player
            var player = $f(player_id),

                playButton = document.getElementById('playButton'),
                pauseButton = document.getElementById('pauseButton'),
                unloadBUtton = document.getElementById('unloadButton');

            /**
             * Attach event listeners.
             *
             * If you're using a javascript framework like jQuery or Mootools
             * you'll probably use their addEvent method to add the click events.
             * Here we're just using the standard W3C addEventListener method. If
             * you need IE8 support, you'll need to use attachEvent for IE8 and
             * addEventListener for everything else (or just use jQuery or MooTools).
             */

            playButton.addEventListener('click', function() {
                player.api('play');
            });

            pauseButton.addEventListener('click', function() {
                player.api('pause');
            });

            unloadButton.addEventListener('click', function() {
                player.api('unload');
            });
        }

        window.addEventListener('load', function() {
            //Attach the ready event to the iframe
            $f(document.getElementById('player')).addEvent('ready', ready);
        });
    </script>
    
  
    
    
<?php snippet('footer') ?>