<?php snippet('header') ?>

<!--<div id="header-image-container">
    <div id="header-image">
        <img src="<?php echo $page->header_image() ?>" alt="<?php echo $page->header_image_title() ?>" title="<?php echo $page->header_image_title() ?>" />
    </div>
</div>-->
<div class="content full-new">
    <?php echo kirbytext($page->text()) ?>
    <ul id="home-boxes" class="about">
        <li class="first">
            <h1><?php echo $page->box_1_title() ?></h1>
            <p><?php echo $page->box_1_description() ?></p>
            <a class="button grey" href="<?php echo $page->box_1_button_link() ?>"><?php echo $page->box_1_button_text() ?></a>
        </li>
        <li class="second">
            <h1><?php echo $page->box_2_title() ?></h1>
            <p><?php echo $page->box_2_description() ?></p>
            <a class="button grey" href="<?php echo $page->box_2_button_link() ?>"><?php echo $page->box_2_button_text() ?></a>
        </li>
        <li class="third">
            <h1><?php echo $page->box_3_title() ?></h1>
            <p><?php echo $page->box_3_description() ?></p>
            <a class="button grey" href="<?php echo $page->box_3_button_link() ?>"><?php echo $page->box_3_button_text() ?></a>
        </li>


        <div class="clear"><!-- --></div>
    </ul>
</div>
<div class="clear"><!-- --></div>
<?php snippet('footer') ?>