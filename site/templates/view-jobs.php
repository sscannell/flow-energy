<?php global $post; include('emails/emailFunctions.php');?>
<?php snippet('header') ?>

<?php $debug = (isset($_GET['debug'])&&$_GET['debug']=='true'); ?>

<div class="content full-new">

    <?php echo kirbytext($page->text()) ?>





    <ul id="jobs-list" style="list-style: none; padding:0;">
        <?php $i=0; foreach($page->children() as $child):?>
            <li class="job-post">


                <h2><?php echo $child->title();?></h2>
                <p><?php echo $child->intro_text();?></p>
                <a href="#" class="job-link button blue">Read More</a>
                <div class="job-body" style="margin-bottom: 40px;"><?php echo kirbytext($child->text());?></div>

            </li>
            <?php endforeach;?>

        <div class="clear"><!-- --></div>
    </ul>

    <div class="line" id="form-start"><!-- --></div>
    <div class="clear"><!-- --></div>
    <?php if(!empty($_SESSION['success']) && $_SESSION['success'] == true): $_SESSION['success'] = false; ?>
        <div class="right first">
            <p class="email-success">Thanks for your application.  We�ll be in touch soon.</p>
            
        </div>
    <?php else:?>
    <?php errorsBox();?>
    <form class="careers" method="post" enctype="multipart/form-data" action="<?php echo url("emails/teams-and-roles.php");?>">
        <label>Enter job reference</label> 
        <input <?php if(!empty($errors)) findError('position' , $errors );?> type="text" name="position" value="<?php echo (!empty($post['position'])) ? $post['position'] : '';?>" />
        <label>Your name</label>
        <input <?php if(!empty($errors)) findError('name' , $errors );?> type="text" name="name" value="<?php echo (!empty($post['name'])) ? $post['name'] : '';?>" />
        <label>Your email address</label>
        <input <?php if(!empty($errors)) findError('email' , $errors );?> type="text" name="email" value="<?php echo (!empty($post['email'])) ? $post['email'] : '';?>" />


            <label>Upload a covering letter</label>
                <div>
                    <div class="file cl"><div class="filename">choose your file</div></div>
                    <input type="file" id="cl-file" name="cl" />

                </div>




        <label>Upload your cv</label>
        <div>
            <div class="file cv"><div class="filename">choose your file</div></div>
            <input type="file" id="cv-file" name="cv" />
        </div>

        <input type="submit" class="submit" value="Submit Application" />
        <script>
        $(document).ready(function(){
            $(".file.cv").click(function(){
                $("#cv-file").click();
            });
            $(".file.cl").click(function(){
                $("#cl-file").click();
            });
            
            
          $('input[type=file]').change(function(e){
              $in=$(this);
              var path = $in.val()
              path = path.replace(/\\/g,'/').replace( /.*\//, '' );

              $(this).parent().find(".filename").html(path);
            });

            $('.job-body').hide();
            $('a.job-link').click(function(ev){
                ev.preventDefault();
                $('.job-body').slideUp();
                $(this).parent().find('.job-body').slideDown()
            });
        })
        </script>
        
        <div class="clear"><!-- --></div>
    </form>
    <?php endif;?>
    <div class="clear"><!-- --></div>
        
</div>
<div class="clear"><!-- --></div>
<?php snippet('footer') ?>