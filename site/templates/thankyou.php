<?php snippet('header') ?>
<div class="content full-new">
    <?php if($site->uri()->path()->last() == 'submit-a-meter-reading'):?>
    <div class="img-right">
        <img src="<?php echo url("assets/images/flow-meter.png");?>" />
    </div>
    <?php endif;?>
    <div class="leftcontent">
    <?php echo kirbytext($page->text()) ?>
    
    </div>
    
    <div class="imageright">
    <img src="../../assets/images/thankyou-flow.jpg">
    </div>
    
</div>
<div class="clear"><!-- --></div>
<?php snippet('footer') ?>