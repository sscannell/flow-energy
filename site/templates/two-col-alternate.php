<?php snippet('header') ?>
<div class="content full-new go-with-flow about-flow">
    <?php echo kirbytext($page->text()) ?>
    <br /><br />
	<div class="left-content small-content <?php if($page->title() == 'Who are flow?'):?>first<?php endif;?>">
        <img src="<?php echo $page->box_1_image();?>" alt="<?php echo $page->box_1_image_alt();?>" title="<?php echo $page->box_1_image_alt();?>"/>
    </div>
	<div class="right-content big-content">
	<?php echo kirbytext($page->box_1_description());?>
    <?php if($page->box_1_button_text() != ''):?>    
	<a class="button blue" href="<?php echo $page->box_1_button_link();?>"><?php echo $page->box_1_button_text();?></a>
    <?php endif;?>    
    </div>
        
	<div class="clear"><!-- --></div><br /><br /><br />

    <div class="left-content big-content">
        <?php echo kirbytext($page->box_2_description());?>
        <?php if($page->box_2_button_text() != ''):?>    
    	<a class="button blue" href="<?php echo $page->box_2_button_link();?>"><?php echo $page->box_2_button_text();?></a>
        <?php endif;?>  
    </div>
	<div class="right-content small-content">
        <img src="<?php echo $page->box_2_image();?>" alt="<?php echo $page->box_2_image_alt();?>" title="<?php echo $page->box_2_image_alt();?>" />
    </div>
	<div class="clear"><!-- --></div>
	<?php if($page->box_3_description()):?>
    <br /><br /><br />
    <div class="left-content small-content">
        <img src="<?php echo $page->box_3_image();?>" alt="<?php echo $page->box_3_image_alt();?>" title="<?php echo $page->box_3_image_alt();?>" />
    </div>
    <div class="right-content big-content">
        <?php echo kirbytext($page->box_3_description());?>
        <?php if($page->box_3_button_text() != ''):?>    
    	<a class="button blue" href="<?php echo $page->box_3_button_link();?>"><?php echo $page->box_3_button_text();?></a>
        <?php endif;?>    
     </div>
    <div class="clear"><!-- --></div>
    <?php endif;?>
    
    <?php if($page->box_4_description()):?>
    <br /><br /><br />
     <div class="left-content big-content">
        <?php echo kirbytext($page->box_4_description());?>
        <?php if($page->box_4_button_text() != ''):?>    
    	<a class="button blue" href="<?php echo $page->box_4_button_link();?>"><?php echo $page->box_4_button_text();?></a>
        <?php endif;?> 
    </div>
    <div class="right-content small-content">
           <img src="<?php echo $page->box_4_image();?>" alt="<?php echo $page->box_4_image_alt();?>" title="<?php echo $page->box_4_image_alt();?>" />
     </div>
    <div class="clear"><!-- --></div>
    <?php endif;?>
    
    <?php if($page->box_5_description()):?>
    <br /><br /><br />
     <div class="left-content small-content">
        <img src="<?php echo $page->box_5_image();?>" alt="<?php echo $page->box_5_image_alt();?>" title="<?php echo $page->box_5_image_alt();?>" />
    </div>
    <div class="right-content big-content">
        <?php echo kirbytext($page->box_5_description());?>
        <?php if($page->box_5_button_text() != ''):?>    
    	<a class="button blue" href="<?php echo $page->box_5_button_link();?>"><?php echo $page->box_5_button_text();?></a>
        <?php endif;?>    
     </div>
    <div class="clear"><!-- --></div>
    <?php endif;?>
    
    
</div>
<div class="clear"><!-- --></div>
<?php snippet('footer') ?>