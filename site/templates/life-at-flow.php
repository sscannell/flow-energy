<?php snippet('header') ?>
<div id="header-image-container">
    <div id="header-image">
        <img src="<?php echo $page->header_image() ?>" alt="<?php echo $page->header_image_title() ?>" title="<?php echo $page->header_image_title() ?>" />
    </div>
</div>
<div class="content full-new">
    <?php if($site->uri()->path()->last() == 'submit-a-meter-reading'):?>
    <div class="img-right">
        <img src="<?php echo url("assets/images/flow-meter.png");?>" />
    </div>
    <?php endif;?>
    <?php echo kirbytext($page->text()) ?>
    
</div>
<div class="clear"><!-- --></div>
<?php snippet('footer') ?>