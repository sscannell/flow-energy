<?php global $post; include('emails/emailFunctions.php');?>
<?php snippet('header') ?>
<div class="content full-new">
    <?php if(!empty($_SESSION['success']) && $_SESSION['success'] == true): $_SESSION['success'] = false; ?>
                <div class="right first">
                    <p  class="email-success">Thanks! We'll put you on the priority list and we'll be in touch later this year.</p>

                </div>
            <?php else:?>
            
            <div class="left first">
            </div>
            <div class="right first">
                <?php echo kirbytext($page->text()) ?>
                
            </div>
            <div class="clear"><!-- --></div>
            <div class="line" id="form-start"><!-- --></div>
            <?php errorsBox();?>
            <form id="register-form" method="post" action="<?php echo url('emails/register-for-interest.php'); ?>">
                <div class="left">
                    <label>Title</label> 
                </div>
                <div class="right">
                    <div class="select-box-big register">
                    <select name="title" class="styled" >
                        <option<?php echo (!empty($post['title']) && $post['title']=='Mr') ? ' selected="selected"' : '';?> value="Mr">Mr.</option>
                        <option<?php echo (!empty($post['title']) && $post['title']=='Mrs') ? ' selected="selected"' : '';?> value="Mrs">Mrs.</option>
                        <option<?php echo (!empty($post['title']) && $post['title']=='Miss') ? ' selected="selected"' : '';?> value="Miss">Miss.</option>
                        <option<?php echo (!empty($post['title']) && $post['title']=='Ms') ? ' selected="selected"' : '';?> value="Ms">Ms.</option>
                    </select>
                    </div>
                   
                </div>

                    <div class="clear"><!-- --></div>
                    <div class="left">
                        <label>First name</label> 
                    </div>
                    <div class="right">
                        <input <?php if(!empty($errors)) findError('first' , $errors );?> type="text" name="first" value="<?php echo (!empty($post['first'])) ? $post['first'] : '';?>"  />
                    </div>

<div class="clear"><!-- --></div>
                    <div class="left">
                        <label>Last name</label> 
                    </div>
                    <div class="right">
                        <input <?php if(!empty($errors)) findError('last' , $errors );?> type="text" name="last" value="<?php echo (!empty($post['last'])) ? $post['last'] : '';?>"  />
                    </div>
					
<div class="clear"><!-- --></div>

                    <div class="left">
                        <label>Email address</label> 
                    </div>
                    <div class="right">
                        <input <?php if(!empty($errors)) findError('email' , $errors );?> type="text" name="email" value="<?php echo (!empty($post['email'])) ? $post['email'] : '';?>"  />
                    </div>

<div class="clear"><!-- --></div>

                    <div class="left">
                        <label>Confirm email address</label> 
                    </div>
                    <div class="right">
                        <input <?php if(!empty($errors)) findError('email' , $errors );?> type="text" name="confirm_email" value="<?php echo (!empty($post['confirm_email'])) ? $post['confirm_email'] : '';?>"  />
                    </div>

<div class="clear"><!-- --></div>

                    <div class="left">
                        <label>Phone number</label> 
                    </div>
                    <div class="right">
                        <input type="text" name="phone" value="<?php echo (!empty($post['phone'])) ? $post['phone'] : '';?>"  />
                    </div>

<div class="clear"><!-- --></div>

                    <div class="left">
                        <label>Post code</label> 
                    </div>
                    <div class="right">
                        <input <?php if(!empty($errors)) findError('post_code' , $errors );?> type="text" name="post_code" value="<?php echo (!empty($post['post_code'])) ? $post['post_code'] : '';?>"  />
                    </div>

<div class="clear"><!-- --></div>

                
                    <div class="left">
                        <!-- -->             
                    </div>
                    <div class="right">
                        <input type="submit" class="submit" value="Keep me informed" />
                    </div>
                    <div class="clear"><!-- --></div>
            </form>
            <?php endif;?>
			<div class="clear"><!-- --></div>
    
    
    
</div>
<div class="clear"><!-- --></div>
<?php snippet('footer') ?>