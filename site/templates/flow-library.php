<?php snippet('header') ?>
<div class="content full-new">
    <?php echo kirbytext($page->text()) ?>
   
    <ul id="useful-info-boxes">
         <?php $i=0; foreach($page->children() as $child):?>
            <li class="info-box-<?php echo ++$i;?>">
                
                <div class="img">
                    <?php foreach($child->images() as $image): ?>
                        <img src="<?php echo $image->url() ?>"  alt="<?php echo $image->name() ?>" />
                    <?php endforeach ?>
                </div>
                <h1><?php echo $child->title();?></h1>
                <p><?php echo $child->text();?></p>
                <a class="button" href="<?php echo $child->button_link();?>"><?php echo $child->button_text();?></a>
            </li>
        <?php endforeach;?>

        <div class="clear"><!-- --></div>
    </ul>
</div>
<div class="clear"><!-- --></div>
<?php snippet('footer') ?>