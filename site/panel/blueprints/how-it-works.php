<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Page
pages: true
files: true
fields:
  title: 
    label: Title
    type:  text
  text: 
    label: Text
    type:  textarea
    size:  medium
  stage_1:
    label: Deal Stage 1
    type: textarea
    size: medium
  stage_2:
    label: Deal Stage 2
    type: textarea
    size: medium
  stage_3:
    label: Deal Stage 3
    type: textarea
    size: medium
  stage_4:
    label: Deal Stage 4
    type: textarea
    size: medium
  stage_5:
    label: Deal Stage 5
    type: textarea
    size: medium
  meta_description:
    label: Page Title
    type: text
    size: medium   
  meta_description_description:
    label: Meta Description
    type: textarea
    size: medium      