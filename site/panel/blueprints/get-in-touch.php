<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Page
pages: true
files: true
fields:
  title: 
    label: Title
    type:  text
  text: 
    label: Text
    type:  textarea
    size:  large
  col_1_title:
    label: Column 1 Title
    type: text
  call_us_details: 
    label: Column 1 Description
    type:  textarea
    size:  medium
  opening_times: 
    label: Opening Times
    type:  textarea
    size:  medium
  col_2_title:
    label: Column 2 Title
    type: text
  faq_details: 
    label: Column 2 Description
    type:  textarea
    size:  medium
  col_3_title:
    label: Column 3 Title
    type: text
  email_details: 
    label: Column 3 Description
    type:  textarea
    size:  medium
  meta_description:
    label: Page Title
    type: text
    size: medium   
  meta_description_description:
    label: Meta Description
    type: textarea
    size: medium      