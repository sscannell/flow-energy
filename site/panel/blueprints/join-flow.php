<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Page
pages: true
files: true
fields:
  title: 
    label: Title
    type:  text
  text: 
    label: Text
    type:  textarea
    size:  large
  call:
    label: Call to action
    type:  text
    size:  large
  tagline:
    label: To-do
    type:  text
    size:  large
  item_1:
    label: Point 1
    type:  text
    size:  large
  item_2:
    label: Point 2
    type:  text
    size:  large
  item_3:
    label: Point 3
    type:  text
    size:  large
    
    
    
  meta_description:
    label: Page Title
    type: text
    size: medium   
  meta_description_description:
    label: Meta Description
    type: textarea
    size: medium      