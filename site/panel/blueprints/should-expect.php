<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Page
pages: true
files: true
fields:
  title: 
    label: Title
    type:  text
  text: 
    label: Text
    type:  textarea
    size:  large
  intro:
    label: Top Right Text
    type: textarea
    size: medium
  quote:
    label: Customer Quote
    type: textarea
    size: medium
  quote_author:
    label: Customer Name
    type: text
  list1:
    label: Tick 1
    type: text
  list2:
    label: Tick 2
    type: text
  list3:
    label: Tick 3
    type: text
  list4:
    label: Tick 4
    type: text
  list5:
    label: Tick 5
    type: text
  meta_description:
    label: Page Title
    type: text
    size: medium   
  meta_description_description:
    label: Meta Description
    type: textarea
    size: medium      