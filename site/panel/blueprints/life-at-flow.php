<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Page
pages: true
files: true
fields:
  header_image:
    label: Header Image
    type: text
    size: medium
  header_image_title:
    label: Header Image Title
    type: text
    size: medium
  title: 
    label: Title
    type:  text
  text: 
    label: Text
    type:  textarea
    size:  large
  meta_description:
    label: Page Title
    type: text
    size: medium   
  meta_description_description:
    label: Meta Description
    type: textarea
    size: medium      