<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Page
pages: true
files: true
fields:
  title: 
    label: Title
    type:  text
  redirect_url:
    label: Redirect URL
    type: text
    size: medium
  meta_description:
    label: Page Title
    type: text
    size: medium   
  meta_description_description:
    label: Meta Description
    type: textarea
    size: medium      