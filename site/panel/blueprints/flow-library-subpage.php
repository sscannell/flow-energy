<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Page
pages: true
files: true
fields:
  title: 
    label: Title
    type:  text
  text: 
    label: Text
    type:  textarea
    size:  large
  button_text:
    label: Button Text
    type:  text
    size:  medium
  button_link:
    label: Button Link
    type:  text
    size:  medium
  meta_description:
    label: Page Title
    type: text
    size: medium   
  meta_description_description:
    label: Meta Description
    type: textarea
    size: medium      