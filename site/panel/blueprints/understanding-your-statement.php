<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Page
pages: true
files: true
fields:
  title: 
    label: Title
    type:  text
  text: 
    label: Text
    type:  textarea
    size:  large
  statement_img_1: 
    label: 1 - Statement Image
    type:  text
    size:  large
  statement_1: 
    label: 1 - Statement Description
    type:  textarea
    size:  large
    
  statement_img_2: 
    label: 2 - Statement Image
    type:  text
    size:  large
  statement_2: 
    label: 2 - Statement Description
    type:  textarea
    size:  large
    
  statement_img_3: 
    label: 3 - Statement Image
    type:  text
    size:  large
  statement_3: 
    label: 3 - Statement Description
    type:  textarea
    size:  large
  meta_description:
    label: Page Title
    type: text
    size: medium   
  meta_description_description:
    label: Meta Description
    type: textarea
    size: medium      