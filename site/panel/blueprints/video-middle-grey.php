<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Page
pages: true
files: true
fields:
  title: 
    label: Title
    type:  text
  text: 
    label: Text
    type:  textarea
    size:  large
  meet_flow_sub: 
    label: Text
    type:  textarea
    size:  medium
  benefits_01:
    label: Benefits 01 Text
    type: textarea
    size: medium
  benefits_02:
    label: Benefits 02 Text
    type: textarea
    size: medium
  benefits_03:
    label: Benefits 03 Text
    type: textarea
    size: medium
  the_deal: 
    label: Text
    type:  textarea
    size:  large
  meta_description:
    label: Page Title
    type: text
    size: medium   
  meta_description_description:
    label: Meta Description
    type: textarea
    size: medium      