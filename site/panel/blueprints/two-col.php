<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Page
pages: true
files: true
fields:
  title: 
    label: Title
    type:  text
  
  text:
    label: Text
    type: textarea
    size: medium
    
  box_1_image:
    label: 1 - Image
    type: text
    size: medium
  box_1_image_alt:
    label: 1 - Image Title
    type: text
    size: medium
  box_1_description:
    label: 1 - Description
    type: textarea
    size: medium
  box_1_button_text:
    label: 1 - Button Text
    type: text
    size: medium
  box_1_button_link:
    label: 1 - Button Link
    type: text
    size: medium
    
  box_2_image:
    label: 2 - Image
    type: text
    size: medium
  box_2_image_alt:
    label: 2 - Image Title
    type: text
    size: medium
  box_2_description:
    label: 2 - Description
    type: textarea
    size: medium
  box_2_button_text:
    label: 2 - Button Text
    type: text
    size: medium
  box_2_button_link:
    label: 2 - Button Link
    type: text
    size: medium
    
  box_3_image:
    label: 3 - Image
    type: text
    size: medium
  box_3_image_alt:
    label: 3 - Image Title
    type: text
    size: medium
  box_3_description:
    label: 3 - Description
    type: textarea
    size: medium
  box_3_button_text:
    label: 3 - Button Text
    type: text
    size: medium
  box_3_button_link:
    label: 3 - Button Link
    type: text
    size: medium
  
  box_4_image:
    label: 4 - Image
    type: text
    size: medium
  box_4_image_alt:
    label: 4 - Image Title
    type: text
    size: medium
  box_4_description:
    label: 4 - Description
    type: textarea
    size: medium
  box_4_button_text:
    label: 4 - Button Text
    type: text
    size: medium
  box_4_button_link:
    label: 4 - Button Link
    type: text
    size: medium
    
  box_5_image:
    label: 5 - Image
    type: text
    size: medium
  box_5_image_alt:
    label: 5 - Image Title
    type: text
    size: medium
  box_5_description:
    label: 5 - Description
    type: textarea
    size: medium
  box_5_button_text:
    label: 5 - Button Text
    type: text
    size: medium
  box_5_button_link:
    label: 5 - Button Link
    type: text
    size: medium  
  
  
  meta_description:
    label: Page Title
    type: text
    size: medium   
  meta_description_description:
    label: Meta Description
    type: textarea
    size: medium      