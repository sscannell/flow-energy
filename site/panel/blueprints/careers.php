<?php if(!defined('KIRBY')) exit ?>

# default blueprint

title: Page
pages: true
files: true
fields:
  title: 
    label: Title
    type:  text
  
  header_image:
    label: Header Image
    type: text
    size: medium
  header_image_title:
    label: Header Image Title
    type: text
    size: medium
  
  text:
    label: Text
    type: textarea
    size: medium
    
  box_1_title:
    label: 1 - Box Title
    type: text
    size: medium
  box_1_description:
    label: 1 - Description
    type: textarea
    size: medium
  box_1_button_text:
    label: 1 - Button Text
    type: text
    size: medium
  box_1_button_link:
    label: 1 - Button Link
    type: text
    size: medium
    
  box_2_title:
    label: 2 - Box Title
    type: text
    size: medium
  box_2_description:
    label: 2 - Description
    type: textarea
    size: medium
  box_2_button_text:
    label: 2 - Button Text
    type: text
    size: medium
  box_2_button_link:
    label: 2 - Button Link
    type: text
    size: medium
    
  box_3_title:
    label: 3 - Box Title
    type: text
    size: medium
  box_3_description:
    label: 3 - Description
    type: textarea
    size: medium
  box_3_button_text:
    label: 3 - Button Text
    type: text
    size: medium
  box_3_button_link:
    label: 3 - Button Link
    type: text
    size: medium
  meta_description:
    label: Page Title
    type: text
    size: medium   
  meta_description_description:
    label: Meta Description
    type: textarea
    size: medium      