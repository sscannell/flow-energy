<!DOCTYPE html>
<html lang="en">
<head>
    <?php if($page->meta_description() != ""):?>
    <title><?php echo html($page->meta_description()) ?></title>
    <?php else:?>
    <title><?php echo html($site->title()) ?> - <?php echo html($page->title()) ?></title>
    <?php endif;?>
    <meta charset="utf-8" />
    <meta name="description" content="<?php echo html($page->meta_description()) ?>" />
    <meta name="keywords" content="<?php echo html($site->keywords()) ?>" />
    <meta name="robots" content="index, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="author" content="Flow" />
    <?php echo css('assets/styles/styles.css') ?>
    <!--[if IE 7]>
         <?php echo css('assets/styles/ie7.css') ?>
    <![endif]-->
    <?php echo css('assets/styles/colorbox.css') ?>
    <?php echo css('assets/javascript/form.css') ?>
    <?php echo css('assets/javascript/flexslider.css') ?>
    <?php echo css('assets/javascript/jquery.fancybox.css') ?>
    <?php echo js('assets/javascript/jquery-1.8.3.min.js') ?>
    <?php echo js('assets/javascript/jquery.colorbox.js') ?>
    <?php echo js('assets/javascript/jquery.flexslider-min.js') ?>
    <?php echo js('assets/javascript/custom-form-elements.js') ?>
    <?php echo js('assets/javascript/jquery.fancybox.pack.js') ?>
    <?php echo js('assets/javascript/jquery.placeholder.min.js') ?>


	<script>
        $(document).ready(function(){
            //Examples of how to assign the Colorbox event to elements
				$(".vimeo").colorbox({iframe:true, innerWidth:895, innerHeight:503});
        });
    </script>

    <script>
       $(document).ready(function(){
            $(".fancybox").fancybox();
            $('input, textarea').placeholder();

            $("#menu li.dropdown").click(function(e){
                e.stopPropagation();
                if($(this).hasClass('open')){
                    $("#menu li.dropdown").addClass('hoverEnabled');
                    $(this).removeClass('open');
                    $("div.menu-hover").removeClass('open')
                } else {
                    
                    $("#menu li.dropdown").removeClass('hoverEnabled');
                    $(this).addClass('open');
                    $("div.menu-hover").addClass('open')
                }                    
            });
            $("#accept-cookies").click(function(e){
                e.preventDefault();
                $.ajax({
                    url: "<?php echo url("assets/javascript/agree-cookies.php");?>"
                }).done(function(data) {
                    //alert(data);
                    $(".cookies").slideUp(300);
                    $("#header.cookie").animate({marginTop : 0},300,function(){
                        $("#header.cookie").removeClass('cookie');
                    })
                });    
            })
            
            
            $("body, #menu li a").click(function(){
                $("#menu li.dropdown").addClass('hoverEnabled');
                $("#menu li.dropdown").removeClass('open');
                $("div.menu-hover").removeClass('open');
            })
            
            $("#menu-hidden li a").click(function(){
                if($(this).parent().find('ul').hasClass('open')){
                    $(this).parent().find('ul').slideUp().removeClass('open');
                } else {
                    $("#menu-hidden li ul.open").slideUp().removeClass('open');
                    $(this).parent().find('ul').slideDown().addClass('open');
                }
               
            })
            
            $("#menu-hidden-button").click(function(){
               if($(this).hasClass('open')){
                    $(this).removeClass('open');
                    $("#menu-hidden").slideDown();
               }
               else {
                    $(this).addClass('open');
                    $("#menu-hidden").slideUp();
                   
               }
            })
            
            $(
				'.frequently-asked-questions em,\
				.switching-to-flow-2 em,\
				.account-number em,\
				.direct-debits em,\
				.making-a-payment em,\
				.smart-meters em,\
				.the-Flow-boiler em,\
				.accordion em,\
				.contact-form em,\
				.flow-boiler-faqs em'
			).css('display','block').slideUp().parent().addClass('faq-p');
			
            $(
				'.frequently-asked-questions br,\
				.switching-to-flow-2 br,\
				.account-number br,\
				.direct-debits br,\
				.making-a-payment br,\
				.smart-meters br,\
				.the-Flow-boiler br,\
				.accordion br,\
				.flow-boiler-faqs br'
			).remove();
			
			/*$('body.frequently-asked-questions em').css('display','block').slideUp().parent().addClass('faq-p');
            $('body.frequently-asked-questions br').remove();*/
			
            $('a.faq-button').click(function(e){
                e.preventDefault();
                $(this).toggleClass('active');
                $(this).next().slideToggle();
                
               
                
            })
         })               
        
    </script>

</head>

<body class="<?php echo ($page->isHomePage()) ? 'home' : $site->uri()->path()->last() ?>">
<a href="http://quote.flowenergy.uk.com/" class="quoteButton">
    	<img class="iconCalc" src="<?php echo url('images/icon-calculator.png');?>" />Get a home energy quote
    </a>
    <div id="header" <?php if(!(!empty($_COOKIE['agreeCookies']) && $_COOKIE['agreeCookies'] == 1)):?> class="cookie"<?php endif;?>>
        <?php $baseUrl = '';?>
        <a id="login" href="https://your.flowenergy.uk.com/" target="_blank">Login to Your Flow</a>
        <!--<a id="login" class="fancybox" href="#login-message">Login to Your Flow</a>-->
        <div id="header-share">
            <a class="twitter" target="_blank" href="https://twitter.com/@FlowEnergyUK"><!-- --></a>
            <a class="facebook" target="_blank" href="http://www.facebook.com/pages/Flow-Energy/208773079267543"><!-- --></a>

        </div>
        <a id="menu-hidden-button" class="open" href="javascript:;"><img src="<?php echo url('assets/images/menu-button.png');?>" /></a>
        
        <?php snippet('mobile-menu') ?>
        
        
        <a id="logo" href="<?php echo url('/') ?>"><img src="<?php echo url("assets/images/flow-logo-tm.png");?>" alt="Flow Energy logo" title="Flow Energy logo" /></a>
        <?php snippet('menu') ?>
        <div class="clear"><!-- --></div>
</div>
<div class="menu-hover">
        

  