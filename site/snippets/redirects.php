<?php

$urlToRedirect = $_SERVER['REQUEST_URI']; // this is the requested URL 

if (false !== strpos($urlToRedirect, 'jacobbaileytest')) {
    
    $siteURL = "flowenergy.uk.com";

} else {

	$siteURL = "www.flowenergy.uk.com";

}

$urlList = array (/*    'requested url'            =>    'redirect url',             */    
                        "/quote-form.php"  =>  "http://quote.flowenergy.uk.com/",
						"/meet-flow"  =>  "http://$siteURL/the-flow-boiler/meet-flow",
						"/submit-a-reading.php"  =>  "http://www.flowenergy.uk.com/meter-readings/submit-a-meter-reading",
						"/ways-to-pay.php"  =>  "http://$siteURL/help/frequently-asked-questions",
						"/understanding-your-bill.php"  =>  "http://$siteURL/help/frequently-asked-questions/understanding-your-statement",
						"/understanding-your-online-account.php"  =>  "http://www.flowenergy.uk.com/pdf/Flow-User-Manual.pdf",
						"/faq.php"  =>  "http://$siteURL/help/frequently-asked-questions",
						"/probate.php"  =>  "http://$siteURL/help/frequently-asked-questions/probate",
						"/moving-home.php"  =>  "http://$siteURL/help/frequently-asked-questions/moving-home",
						"/useful-info.php"  =>  "http://$siteURL/help/flow-library",
						"/emergencies.php"   =>  "http://$siteURL/help/emergency-contacts",
						"/safety-advice.php"  =>  "http://$siteURL/help/frequently-asked-questions/safety-advice",
						"/how-to-switch.php"  =>  "http://$siteURL/help/frequently-asked-questions",
						"/refer-a-friend.php"  =>  "http://www.flowenergy.uk.com",
						"/energy-saving-tips.php"  =>  "http://$siteURL/help/frequently-asked-questions/energy-saving-tips",
						"/careers.php"  =>  "http://$siteURL/help/careers",
						"/our-promise.php"  =>  "http://$siteURL/great-service/what-you-should-expect",
						"/media-centre.php"  =>  "http://$siteURL/help/media-centre",
						"/energetix-group.php"  =>  "http://www.flowgroup.uk.com",
						"/get-in-touch/ways-to-contact-us"  =>  "http://$siteURL/get-in-touch/contact-us",
						"/emergency-numbers.php"  =>  "http://$siteURL/help/emergency-contacts",
						"/register-for-interest.php"  =>  "http://$siteURL/the-flow-boiler/register-interest",
						"/make-a-complaint.php"  =>  "http://$siteURL/help/complaints-procedure",
						"/social-media.php"  =>  "http://www.flowenergy.uk.com"
                );

if(array_key_exists("$urlToRedirect", $urlList)) {
    header("Location: {$urlList[$urlToRedirect]}");
} 
