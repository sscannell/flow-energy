<div id="footer-container">
            <div id="footer">
                <ul id="footer-menu">
                    <li <?php if($site->uri()->path()->last() == '/') : echo 'class="active"'; endif ;?>><a href="<?php echo url('/') ?>">Home</a></li>
                    <li <?php if($site->uri()->path()->last() == 'privacy') : echo 'class="active"'; endif ;?>><a href="<?php echo url('/privacy') ?>">Privacy</a></li>
                    <li <?php if($site->uri()->path()->last() == 'terms-and-conditions') : echo 'class="active"'; endif ;?>><a href="<?php echo url('/terms-and-conditions') ?>">Terms and conditions</a></li>
                    <li <?php if($site->uri()->path()->last() == 'sitemap') : echo 'class="active"'; endif ;?>><a href="<?php echo url('/sitemap') ?>">Sitemap</a></li>
                    <li <?php if($site->uri()->path()->last() == 'legal-information') : echo 'class="active"'; endif ;?>><a href="<?php echo url('/legal-information') ?>">Legal information</a></li>
                    <li <?php if($site->uri()->path()->last() == 'cookies') : echo 'class="active"'; endif ;?>><a href="<?php echo url('/cookies') ?>">Cookies</a></li>
                    <div class="clear"><!-- --></div>
                </ul>
                <!--<div id="footer-share">
                    <a class="twitter" target="_blank" href="https://twitter.com/@FlowEnergyUK"></a>
                    <a class="facebook" target="_blank" href="http://www.facebook.com/pages/Flow-Energy/208773079267543"></a>
					<a class="linkedin" target="_blank" href="http://www.linkedin.com/company/2921904?trk=tyah"></a>
                </div>-->
                <p class="copyright">Flow&copy; 2013. Flow Energy - part of Flowgroup</p>
            </div>
        </div>
        <div class="clear"><!-- --></div>
      </div>  
      
<div class="cookies-container">
    <?php if(!(!empty($_COOKIE['agreeCookies']) && $_COOKIE['agreeCookies'] == 1)):?>
    <div class="cookies">
        <p>We use cookies to improve your experience of our site. If you proceed, you're agreeing that's ok. <a href="<?php echo url('/cookies') ?>">Click here</a> for more information.</p>
        <a id="accept-cookies" href="#"><img src="<?php echo url("assets/images/accept-cookies.png");?>" /></a>
    </div>
    <?php endif;?>

</div>      

<div id="login-message">
    <h1>Your Flow</h1>
	<p>You'll be able to manage your Flow account using our intelligent online portal right here. <br>
	We're just putting the finishing touches to it and we'll let you know as soon as it's ready. 
	For now, if you’d like to supply us with a meter reading, please click <a href="http://www.flowenergy.uk.com/readings/">here</a>.<br>  
	Or, if have any questions, or if you need to change anything on your account, please <a href="http://www.flowenergy.uk.com/get-in-touch/ways-to-contact-us">contact us</a>.</p>
    
</div>

<script>
	$(document).ready(function(){
		$(".quoteButton").animate({top:'0px'}, "slow");
	}); 
</script>

<script>

jQuery.fn.toggleText = function (value1, value2) {
    return this.each(function () {
        var $this = $(this),
            text = $this.text();
 
        if (text.indexOf(value1) > -1)
            $this.text(text.replace(value1, value2));
        else
            $this.text(text.replace(value2, value1));
    });
};

	$(document).ready(function(){
		$(".section0").slideUp();
		$(".section1").slideUp();
		$(".section2").slideUp();
		$(".section3").slideUp();
		$(".section4").slideUp();
		$(".section5").slideUp();
		$(".section6").slideUp();
	}); 
	
	$(".openButton").click(function(){
		$(this).toggleText('Open', 'Close');
	}); 
	
	$("#link0").click(function(){
		$(".section0").slideToggle();
	}); 
	
	$("#link1").click(function(){
		$(this).addClass("linkOpen");
		$(".section1").slideToggle();
		
	}); 
	
	$("#link2").click(function(){
		$(".section2").slideToggle();
	}); 
	
	$("#link3").click(function(){
		$(".section3").slideToggle();
	}); 
	
	$("#link4").click(function(){
		$(".section4").slideToggle();
	}); 
	
	$("#link5").click(function(){
		$(".section5").slideToggle();
	}); 
	
	$("#link6").click(function(){
		$(".section6").slideToggle();
	}); 
		
		
$(document).ready(function(e) {
    
	$(".linkBar").click( function() {
		
		//alert("alright!");
		$('.contact-form-holder').load("../contact-form.html");	
		$
	
		
	});
	
	
	
	
});		
		
			
		
</script>

<script>
	$(document).ready(function(){
		$("#existingForm").css("display","none");	
		$("#existingForm").slideUp();
	}); 

	$(".formBtn").click(function(){
	  $("#existingForm").slideToggle();
	});
</script>


<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-324951-42']);
_gaq.push(['_trackPageview']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script> 
</body>
</html>