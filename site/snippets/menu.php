<ul id="menu">
<?php foreach($pages->visible() as $page): ?>
    <li class="dropdown hoverEnabled  <?php echo ($page->title() == 'The Boiler' || $page->title() == 'About Flow' || $page->title() == 'Get in Touch') ? ' small-menu ' : '' ?> <?php echo ($page->isOpen()) ? 'active' : '' ?>"><a href="javascript:;"><?php echo html($page->title()) ?></a>
    <?php $subpages = $page->children()->visible();?>
    <?php if($subpages && $subpages->count()):?>
        <ul>
        <?php foreach($subpages as $subpage):?>
        <li class="<?php echo ($subpage->isOpen()) ? 'active ' : '' ?>title"><a href="<?php echo ($subpage->redirect_url()) ? $subpage->redirect_url() : $subpage->url() ?>"><?php echo html($subpage->title()) ?></a></li>
        <?php endforeach;?>
        </ul>
    <?php endif;?>

</li>
<?php endforeach; ?>
</ul>
    
