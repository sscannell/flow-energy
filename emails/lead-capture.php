<?php
if($_POST){


    $fname=$_POST['firstname'];
    $lname=$_POST['lastname'];
    $email=$_POST['email'];
    


    //RUN CHECK
    if (isset($_POST["postcode"])) {
        

      // if (preg_match('#^(GIR ?0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]([0-9ABEHMNPRV-Y])?)|[0-9][A-HJKPS-UW]) ?[0-9][ABD-HJLNP-UW-Z]{2})$#', $_POST["postcode"])) && substr($_POST["postcode"]), 0, 2) == 'DN' && in_array(substr($_POST["postcode"]), 2, 2), $accepted_numbers)) {
       
            $postcode=$_POST['postcode'];

        //}       

    }


    $errorMessage='';

    if(empty($fname)||empty($lname)||empty($email)){
        $errorMessage='All fields must be filled in';
    }else{
        $email=filter_var($email, FILTER_VALIDATE_EMAIL);
        if(!$email){
        $errorMessage='Email invalid';
        }
    }

    if(!empty($errorMessage)){
        header('Content-Type: application/json');
        echo json_encode(array('type'=>'error', 'message'=>$errorMessage));
        return;
    }

    function clean_string($string) {
        $bad = array("content-type","bcc:","to:","cc:","href","\n","\r","\r\n");
        return str_replace($bad,"",$string);
    }

    $csvline = clean_string($fname) . ",";
    $csvline .= clean_string($lname) . ",";
    $csvline .= clean_string($email).",";
    $csvline .= clean_string($postcode) . "\n";

    file_put_contents("12bd5_lead-capture.csv", $csvline, FILE_APPEND);

    require_once("send_details.php");

    header('Content-Type: application/json');
    echo json_encode(array('type'=>'success', 'message'=>'Thanks! We\'ll be in touch soon.'));
    
    return;

    
}