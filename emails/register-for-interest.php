<?php
include('emailFunctions.php');
if(empty($_POST['title']) || $_POST['title'] == ''){
    $error[] = array('title' => "Please enter your title");
}
if(empty($_POST['first']) || $_POST['first'] == ''){
    $error[] = array('first' => "Please enter your first name");
}
if(empty($_POST['last']) || $_POST['last'] == ''){
    $error[] = array('last' => "Please enter your last name");
}

if(empty($_POST['email']) || $_POST['email'] == ''){
    $error[] = array('email' => "Please enter your email address");
} elseif (!filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL)) {
    $error[] = array('email' => "Invalid email address");
} elseif ($_POST['email'] != $_POST['confirm_email']) {
    $error[] = array('email' => "Email addresses do not match");   
}

if(empty($_POST['post_code']) || $_POST['post_code'] == ''){
    $error[] = array('post_code' => "Please enter your post code");
}



if(!empty($error)){
    $_SESSION['error'] = $error;
    $_SESSION['post'] = $_POST;
    header("Location: " . baseUrl() . "get-in-touch/register-interest/#form-start");
    die();
}

mysql_query("INSERT INTO register_interest_form(title,first_name,last_name,email,telephone,post_code) VALUES('". mysql_real_escape_string($_POST['title']) ."','". mysql_real_escape_string($_POST['first']) ."','". mysql_real_escape_string($_POST['last']) ."','". mysql_real_escape_string($_POST['email']) ."','". mysql_real_escape_string($_POST['phone']) ."','". mysql_real_escape_string($_POST['post_code']) ."')") or die(mysql_error());

$mandrill = new Mandrill('KouNivonxJEpldwLc6CGcA');

/** ****************************
  * **** FLOW STAFF EMAIL   ****
  * ***************************/

$htmlMessage = '<?xml version="1.0" encoding="utf-8"?>
<prospect>
<title>' . mysql_real_escape_string($_POST['title']) . '</title>
<name>' . mysql_real_escape_string($_POST['first']) . ' ' . mysql_real_escape_string($_POST['last']) . '</name>
<email>' . mysql_real_escape_string($_POST['email']) . '</email>
<contact_number>' . mysql_real_escape_string($_POST['phone']) . '</contact_number>
<post_code>' . mysql_real_escape_string($_POST['post_code']) . '</post_code>
</prospect>';
$message = array(
    'subject' => 'Thank you for being interested in the Flow boiler',
    'from_email' => 'register@flowenergy.uk.com',
    'from_name' => "Flow Energy",
    'html' => $htmlMessage,

    'to' => array(array('email' => "info@flowenergy.uk.com", 'name' => "Flow"))
    );
$mandrill->messages->send($message);

/** ****************************
  * ****   SENDER EMAIL     ****
  * ***************************/
$htmlMessage = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
	<head>
		<meta http-equiv='content-type' content='text/html; charset=UTF-8' />
		<meta name='author' content='Thanks for requesting a quote from Flow Energy' />
	</head>
	<body style='background-color: #f5f6f7; height:100%; margin:0px; padding:0px;'>
		<table width='600px' border='0' cellpadding='0' cellspacing='0' aling='center' style='padding:0 0 0 0; background-color:#ffffff; margin:0 auto;'>
			<tr>
				<td style='padding:50px 40px 32px 50px;'>
					<a id='logo' href='http://www.flowenergy.uk.com' target='_blank'><img src='".baseUrl()."images/flow-logo-tm.png' /></a>
				</td>
			</tr>
			<tr>
				<td style='padding:0 40px 11px 50px;'>
					<h1 style='color:#a5b0b6; font-size:22px; line-height:27px; font-family: helvetica, arial sans-serif; font-weight:bold; padding: 0; margin: 0;'>
						The Flow microCHP gas boiler: so clever it pays for itself
					</h1>
				</td>
			</tr>
			<tr>
				<td style='padding:19px 40px 0 50px;'>
					<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
						Hello ".$_POST['first'].",
					</p>
				</td>
			</tr>
			<tr>
				<td style='padding:19px 40px 0 50px;'>
					<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
						Thanks very much for being interested in the Flow boiler.  You’ll be able to apply for one in the second half of 2014.  We promise it’ll be worth the wait.
					</p>
				</td>
			</tr>
			<tr>
				<td style='padding:19px 40px 0 50px;'>
					<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
						We’ll add you to our priority list and we’ll let you know first when we’re ready to launch, by email.
					</p>
				</td>
			</tr>
			<tr>
				<td style='padding:19px 40px 0 50px;'>
					<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
						<a style='text-decoration: none; color:#67c7e3; font-size:14px; font-weight:bold;' href='http://www.flowenergy.uk.com/uploads/The-Flow-Boiler.pdf' target='_blank'>Click here</a> to download a brochure about the boiler that should answer any questions you might have.
					</p>
				</td>
			</tr>
			<tr>
				<td style='padding:19px 40px 0 50px;'>
					<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
						Thanks again for your interest and we’ll be in touch.
					</p>
				</td>
			</tr>
            
			<tr>
				<td style='padding:36px 40px 0 50px;'>
					<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
						Warm regards,
						<br />
						The Flow Team
					</p>
				</td>
			</tr>
			<tr>
				<td style='padding:41px 40px 0 50px;'>
					<hr style='display: block; height: 1px; border: 0; border-top: 1px solid #f2f2f2; margin: 0; background-color:#f2f2f2; padding: 0;' />
				</td>
			</tr>
			<tr>
				<td style='text-align: center; padding:29px 0 12px 0'>
					<a style='display: inline;' href='https://twitter.com/@FlowEnergyUK' target='_blank'><img src='".baseUrl()."images/twitter-icon.png' /></a>
					<a style='display: inline;' href='http://www.linkedin.com/company/2921904?trk=tyah' target='_blank'><img src='".baseUrl()."images/linkedin-icon.png' /></a>
					<a style='display: inline;' href='http://www.facebook.com/pages/Flow-Energy/208773079267543' target='_blank'><img src='".baseUrl()."images/facebook-icon.png' /></a>
				</td>
			</tr>
			<tr>
				<td style='padding:0 40px 30px 50px;'>
					<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:12px; text-align:center; display:block; line-height:19px; padding: 0; margin: 0;'>
						Flow Energy Ltd is a company registered in England and Wales. Company Number: 7489062 Registered address: 48 Felaw Street, North Felaw Maltings, Ipswich, Suffolk, IP2 8PN.  Tel:  0800 092 0202. <a href='http://www.flowenergy.uk.com'>www.flowenergy.uk.com</a>
					</p>
				</td>
			</tr>
		</table>
		<style type='text/css'>
             a:hover {color:#67c7e3 !important;} 
             a.button:hover {background-color:#96a2a7 !important; color:#ffffff !important;}
		</style>
	</body>

</html>";

$message = array(
    'subject' => 'Thank you for being interested in the Flow boiler',
    'from_email' => 'register@flowenergy.uk.com',
    'from_name' => "Flow Energy",
    'html' => $htmlMessage,

    'to' => array(array('email' => $_POST['email'], 'name' => $_POST['first'] . " " . $_POST['last']))
    );
$mandrill->messages->send($message);

$_SESSION['success'] = true;
header("Location: " . baseUrl() . "get-in-touch/register-interest/");

?>