<?php
include('emailFunctions.php');

$badWords = array('shit','fuck','cunt','bollocks','arsehole');

$_POST['first'] = str_replace($badWords,'',$_POST['first']);
$_POST['last'] = str_replace($badWords,'',$_POST['last']);

if(empty($_POST['supplier']) || $_POST['supplier'] == ''){
    $error[] = array('voucher' => "Please select your reward");
} elseif($_POST['supplier'] == 'voucher' && (empty($_POST['voucher']) || $_POST['voucher'] == '')){
    $error[] = array('voucher' => "Please select a voucher");
}

if(empty($_POST['first']) || $_POST['first'] == ''){
    $error[] = array('first' => "Please enter your first name");
}

if(empty($_POST['last']) || $_POST['last'] == ''){
    $error[] = array('last' => "Please enter your last name");
}

if(empty($_POST['email']) || $_POST['email'] == ''){
    $error[] = array('email' => "Please enter your email address");
} elseif (!filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL)) {
    $error[] = array('email' => "Invalid email address");
}

if(empty($_POST['account']) || $_POST['account'] == ''){
    $error[] = array('account' => "Please enter your account number");
} elseif(strlen(intval($_POST['account'])) != 8){
    $error[] = array('account' => "Please enter a valid account number");
} elseif(substr($_POST['account'],0,2) != '21') {
    $error[] = array('account' => "Please enter a valid account number");
}



//if(empty($_POST['share']) || $_POST['share'] == ''){
//    $error[] = array('share' => "Please select a sharing method");
//}

//if(!empty($_POST['share']) && $_POST['share'] == 'Email'){
    if(empty($_POST['friend_name_1']) || $_POST['friend_name_1'] == ''){
        $error[] = array('friend_name_1' => "Please enter your friend's name");
    }
    if(empty($_POST['friend_email_1']) || $_POST['friend_email_1'] == ''){
        $error[] = array('friend_email_1' => "Please enter your friend's email");
    } elseif (!filter_var( $_POST['friend_email_1'], FILTER_VALIDATE_EMAIL)) {
        $error[] = array('friend_email_1' => "Invalid email address");
    }
    
//}
    

for($i=1;$i<=6;$i++){
    $_POST['friend_name_' . $i] = str_replace($badWords,'',$_POST['friend_name_' . $i]);
    $_POST['friend_last_name_' . $i] = str_replace($badWords,'',$_POST['friend_last_name_' . $i]);
    if(!empty($_POST['friend_email_' . $i]) && !filter_var( $_POST['friend_email_' . $i], FILTER_VALIDATE_EMAIL)){
        $error[] = array('friend_email_' . $i => "Invalid email address");
    } 
}





if(!empty($error)){
    $_SESSION['error'] = $error;
    $_SESSION['post'] = $_POST;
    header("Location: " . baseUrl() . "your-account/refer-a-friend/#form-start");
    die();
}


mysql_query("INSERT INTO refer_a_friend_form(reward,voucher,first_name,last_name,email,account,friend_name_1,friend_email_1,friend_name_2,friend_email_2,friend_name_3,friend_email_3,friend_name_4,friend_email_4,friend_name_5,friend_email_5,friend_name_6,friend_email_6) VALUES('". mysql_real_escape_string($_POST['supplier']) ."','". mysql_real_escape_string($_POST['voucher']) ."','". mysql_real_escape_string($_POST['first']) ."','". mysql_real_escape_string($_POST['last']) ."','". mysql_real_escape_string($_POST['email']) ."','". mysql_real_escape_string($_POST['account']) ."'
,'". mysql_real_escape_string($_POST['friend_name_1'] . " " . $_POST['friend_last_name_1']) ."','". mysql_real_escape_string($_POST['friend_email_1']) ."'
,'". mysql_real_escape_string($_POST['friend_name_2'] . " " . $_POST['friend_last_name_2']) ."','". mysql_real_escape_string($_POST['friend_email_2']) ."'
,'". mysql_real_escape_string($_POST['friend_name_3'] . " " . $_POST['friend_last_name_3']) ."','". mysql_real_escape_string($_POST['friend_email_3']) ."'
,'". mysql_real_escape_string($_POST['friend_name_4'] . " " . $_POST['friend_last_name_4']) ."','". mysql_real_escape_string($_POST['friend_email_4']) ."'
,'". mysql_real_escape_string($_POST['friend_name_5'] . " " . $_POST['friend_last_name_5']) ."','". mysql_real_escape_string($_POST['friend_email_5']) ."'
,'". mysql_real_escape_string($_POST['friend_name_6'] . " " . $_POST['friend_last_name_6']) ."','". mysql_real_escape_string($_POST['friend_email_6']) ."'

)") or die(mysql_error());




$mandrill = new Mandrill('KouNivonxJEpldwLc6CGcA');

/** ****************************
  * ****   SENDER EMAIL     ****
  * ***************************/
 

$htmlMessage = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
	<head>
		<meta http-equiv='content-type' content='text/html; charset=UTF-8' />
		<meta name='author' content='Thanks for requesting a quote from Flow Energy' />
	</head>
	<body style='background-color: #f5f6f7; height:100%; margin:0px; padding:0px;'>
		<table width='600px' border='0' cellpadding='0' cellspacing='0' aling='center' style='padding: 0px; background-color:#ffffff; margin:0 auto;'>
			<tr>
				<td style='padding:50px 40px 32px 50px;'>
					<a id='logo' href='http://www.flowenergy.uk.com' target='_blank'><img src='" . baseUrl() . "images/flow-logo-tm.png' /></a>
				</td>
			</tr>
			<tr>
				<td style='padding:0 40px 21px 50px;'>
					<h1 style='color:#a5b0b6; font-size:22px; line-height:27px; font-family: helvetica, arial sans-serif; font-weight:bold; padding: 0; margin: 0;'>
						Thank you
					</h1>
				</td>
			</tr>
			<tr>
				<td style='padding:0 40px 19px 50px;'>
					<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
						Hi " . $_POST['first'] . ".
					</p>
				</td>
			</tr>
			<tr>
				<td style='padding:0 40px 18px 50px;'>
					<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
						You recently recommended Flow to the people below via our website.
						<br />
						Thank you very much!
					</p>
				</td>
			</tr>
			<tr>
				<td style='padding:0 40px 0 50px;'>
					<table style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;' border='0' cellspacing='1' cellpadding='0' align='left'>";
for($i=1;$i<=6;$i++){
    if(!empty($_POST['friend_email_' . $i])){
                    $htmlMessage .= "<tr>
                    <td style='padding: 0 30px 0 0;'><p style='padding: 0; margin: 0;'><strong>".$_POST['friend_name_' . $i]." ".$_POST['friend_last_name_' . $i]."</strong></p></td><td>
                    <a style='text-decoration: none; color:#67c7e3;' href='#'>".$_POST['friend_email_' . $i]."</a></td></tr>";
    }
}
                
$htmlMessage .= "</table>
</td>
</tr>
<tr>
	<td style='padding:32px 40px 0 50px;'>
		<h1 style='color:#a5b0b6; font-size:22px; line-height:27px; font-family: helvetica, arial sans-serif; font-weight:bold; padding: 0; margin: 0;'>
			What happens now?
		</h1>
	</td>
</tr>
<tr>
	<td style='padding:25px 40px 0 50px;'>
		<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
			<strong>
				It's really simple:
			</strong>
			if any of the people you referred sign up and enter your account number, we'll let you know and we'll send you your chosen reward.
		</p>
	</td>
</tr>
<tr>
	<td style='padding:19px 40px 0 50px;'>
		<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
			There are a few simple and fair terms and conditions attached, which we displayed when you made your referral.
			<a href='" . baseUrl() . "your-account/terms-refer-a-friend' target='_blank' style='text-decoration: none; color:#67c7e3;'>You can take a look at them again here.</a>
		</p>
	</td>
</tr>
<tr>
	<td style='padding:19px 40px 0 50px;'>
		<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
			Thanks again for recommending Flow Energy - and for being a valued customer yourself.
		</p>
	</td>
</tr>
<tr>
	<td style='padding:19px 40px 0 50px;'>
		<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
			Warm regards,
			<br />
			The Flow Team
		</p>
	</td>
</tr>
<tr>
                <td style='padding: 41px 40px 0px 50px;'>
                    <hr style='display: block; height: 1px; border: 0; border-top: 1px solid #f2f2f2; margin: 0; background-color:#f2f2f2; padding: 0;' />
                </td>
				
				<tr>
<tr>
	<td style='text-align: center; padding:29px 40px 12px 50px'>
		<a style='display: inline;' href='https://twitter.com/@FlowEnergyUK' target='_blank'><img src='" . baseUrl() . "images/twitter-icon.png' /></a>
		<a style='display: inline;' href='http://www.linkedin.com/company/2921904?trk=tyah' target='_blank'><img src='" . baseUrl() . "images/linkedin-icon.png' /></a>
		<a style='display: inline;' href='http://www.facebook.com/pages/Flow-Energy/208773079267543' target='_blank'><img src='" . baseUrl() . "images/facebook-icon.png' /></a>
	</td>
</tr>
<tr>
	<td style='padding:0 40px 30px 50px';>
		<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:12px; text-align:center; display:block; line-height:19px; padding: 0; margin: 0;'>
			Flow Energy Ltd is a company registered in England and Wales. Company Number: 7489062 Registered address: 48 Felaw Street, North Felaw Maltings, Ipswich, Suffolk, IP2 8PN.  Tel:  0800 092 0202. <a href='http://www.flowenergy.uk.com'>www.flowenergy.uk.com</a>
		</p>
	</td>
</tr>
</table>
<style type='text/css'>
	
    a:hover {color:#67c7e3 !important;}
    a.button:hover {background-color:#96a2a7 !important; color:#ffffff !important;}

</style>
</body>

</html>
";
$message = array(
    'subject' => 'Thank you for referring Flow Energy',
    'from_email' => 'referrals@flowenergy.uk.com',
    'from_name' => "Flow Energy",
    'html' => $htmlMessage,

    'to' => array(array('email' => $_POST['email'], 'name' => $_POST['first'] . " " . $_POST['last']))
    );
$mandrill->messages->send($message);

/** ****************************
  * ****   FRIEND EMAIL     ****
  * ***************************/

for($i=1;$i<=6;$i++){
    if(!empty($_POST['friend_email_' . $i])){
        $htmlMessage = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
	<head>
		<meta http-equiv='content-type' content='text/html; charset=UTF-8' />
		<meta name='author' content='Thanks for requesting a quote from Flow Energy' />
	</head>
	<body style='background-color: #f5f6f7; height:100%; margin:0px; padding:0px;'>
		<table width='600px' border='0' cellpadding='0' cellspacing='0' aling='center' style='padding: 0; background-color:#ffffff; margin:0 auto;'>
			<tr>
				<td style='padding: 50px 40px 32px 50px;'>
					<a id='logo' href='http://www.flowenergy.uk.com/' target='_blank'><img src='".baseUrl()."images/flow-logo-tm.png' /></a>
				</td>
			</tr>
			<tr>
				<td style='padding: 0 40px 14px 50px;'>
					<h1 style='color:#a5b0b6; font-size:22px; line-height:27px; font-family: helvetica, arial sans-serif; font-weight:normal; padding: 0; margin: 0;'>
						<strong>
							".$_POST['first']." ".$_POST['last']."
						</strong>
						has switched to Flow Energy and thinks you should too.
					</h1>
				</td>
			</tr>
			<tr>
				<td style='padding: 0 40px 14px 50px;'>
					<h1 style='color:#a5b0b6; font-size:22px; line-height:27px; font-family: helvetica, arial sans-serif; font-weight:normal; padding: 0; margin: 0;'>
						Why?
					</h1>
				</td>
			</tr>
			<tr>
				<td style='padding: 0 40px 21px 50px;'>
					<h1 style='color:#a5b0b6; font-size:22px; line-height:27px; font-family: helvetica, arial sans-serif; font-weight:normal; padding: 0; margin: 0;'>
						Well, because...
					</h1>
				</td>
			</tr>
			<tr>
				<td style='padding: 1px 40px 0px 50px;'>
					<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
						1. Flow provides one of the cheapest fixed rate energy tariffs available on the market, meaning you could save &pound;&pound;&pound; on your gas and electricity bills.
					</p>
				</td>
			</tr>
			<tr>
				<td style='padding: 19px 40px 0px 50px;'>
					<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
						2. Flow is a different kind of energy company with unbeatable customer service and some really big ideas.
					</p>
				</td>
			</tr>
			<tr>
				<td style='padding: 19px 40px 0px 50px;'>
					<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
						3. ".$_POST['first']." will receive &pound;30 as a thank you from us when you switch!
					</p>
				</td>
			</tr>
			<tr>
				<td style='padding: 28px 40px 0px 50px;'>
					<h1 style='color:#67c7e3; font-size:22px; font-weight:normal; line-height:27px; font-family: helvetica, arial sans-serif; padding:0px; margin:0px;'>
						Make yourself - and ".$_POST['first']." - smile. Switch today.
					</h1>
				</td>
			</tr>
			<tr>
				<td style='padding: 23px 40px 0px 50px;'>
					<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
						<strong>
							PLEASE NOTE:
						</strong>
						in order for ".$_POST['first']." to receive the &pound;30, you will need to enter ".$_POST['first']."'s account number when prompted during the switching process.
					</p>
				</td>
			</tr>
			<tr>
				<td style='padding: 19px 40px 0px 50px;'>
					<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
						".$_POST['first']."'s account number is:
						<strong>
							".$_POST['account']."
						</strong>
					</p>
				</td>
			</tr>
			<tr>
				<td style='padding: 27px 40px 0px 50px;'>
					<a class='button' style='font-family: helvetica, arial sans-serif; display:block; height:40px; line-height:40px; color:#ffffff; font-size:14px; font-weight:bold; text-decoration:none; -webkit-border-radius: 3px; width:255px; text-align:center; -moz-border-radius: 3px; border-radius: 3px; background-color:#67c7e3;'
					href='http://quote.flowenergy.uk.com/' target='_blank'>Get a quote &amp; switch now</a>
				</td>
			</tr>
			
            <tr>
                <td style='padding: 41px 40px 0px 50px;'>
                    <hr style='display: block; height: 1px; border: 0; border-top: 1px solid #f2f2f2; margin: 0; background-color:#f2f2f2; padding: 0;' />
                </td>
				
				<tr>
					<td style='text-align: center; padding:29px 40px 12px 50px'>
						<a style='display: inline;' href='https://twitter.com/@FlowEnergyUK' target='_blank'><img src='".baseUrl()."images/twitter-icon.png' /></a>
						<a style='display: inline;' href='http://www.linkedin.com/company/2921904?trk=tyah' target='_blank'><img src='".baseUrl()."images/linkedin-icon.png' /></a>
						<a style='display: inline;' href='http://www.facebook.com/pages/Flow-Energy/208773079267543' target='_blank'><img src='".baseUrl()."images/facebook-icon.png' /></a>
					</td>
				</tr>
				<tr>
					<td  style='padding: 0 40px 0px 50px;'>
						<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:12px; text-align:center; display:block; line-height:19px; padding: 0; margin: 0;'>
							Flow Energy Ltd is a company registered in England and Wales. Company Number: 7489062 Registered address: 48 Felaw Street, North Felaw Maltings, Ipswich, Suffolk, IP2 8PN.  Tel:  0800 092 0202. <a href='http://www.flowenergy.uk.com'>www.flowenergy.uk.com</a>
						</p>
					</td>
				</tr>
				<tr>
					<td style='padding: 24px 40px 0px 50px;'>
						<hr style='display: block; height: 1px; border: 0; border-top: 1px solid #f2f2f2; margin: 0; background-color:#f2f2f2; padding: 0;' />
					</td>
				</tr>
				<tr>
					<td style='padding: 19px 40px 40px 50px;'>
						<p style='font-family: helvetica, arial sans-serif; color:#a5b0b6; font-size:14px; line-height:19px;padding: 0; margin: 0;'>
							You're receiving this email because ".$_POST['first']." ".$_POST['last'].", who is a Flow customer, entered your details on our site so we could send you details about Flow Energy’s great rates on gas
							and electricity.
						</p>
					</td>
				</tr>
		</table>
		<style type='text/css'>
			 a:hover {color:#67c7e3 !important;} a.button:hover {background-color:#96a2a7 !important; color:#ffffff !important;}
		</style>
	</body>

</html>
";
        $message = array(
            'subject' => $_POST['first'] . ' has switched to Flow Energy and thinks you should too',
            'from_email' => 'referrals@flowenergy.uk.com',
            'from_name' => $_POST['first'] . " " . $_POST['last'],
            'html' => $htmlMessage,
        
            'to' => array(array('email' => $_POST['friend_email_' . $i], 'name' => $_POST['friend_name_' . $i]))
            );
        $mandrill->messages->send($message);
    }
}



$_SESSION['success'] = true;
$_SESSION['share_method'] = $_POST['share'];
header("Location: " . baseUrl() . "your-account/refer-a-friend/#form-start");

?>