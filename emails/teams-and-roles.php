<?php
include('emailFunctions.php');
include('uploader.php');


if(empty($_POST['position']) || $_POST['position'] == ''){
    $error[] = array('position' => "Please enter a position");
}
if(empty($_POST['name']) || $_POST['name'] == ''){
    $error[] = array('name' => "Please enter your name");
}

if(empty($_POST['email']) || $_POST['email'] == ''){
    $error[] = array('email' => "Please enter your email address");
} elseif (!filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL)) {
    $error[] = array('email' => "Invalid email address");
}

$uploader   =   new Uploader();
$uploader->setDir('../cv-uploads/');
$uploader->setExtensions(array('pdf','doc','docx'));  //allowed extensions list//
$uploader->setMaxSize(3);                          //set max file size to be allowed in MB//

if($uploader->uploadFile('cv')){   //txtFile is the filebrowse element name //     
    $cv  =   $uploader->getUploadName(); //get uploaded file name, renames on upload//

}else{ //upload failed
    $fileError = $uploader->getMessage(); //get upload error message 
    $error[] = array('file' => $fileError);
}

$cv = baseUrl() . "cv-uploads/" . $cv;


    $uploader   =   new Uploader();
    $uploader->setDir('../cv-uploads/');
    $uploader->setExtensions(array('pdf','doc','docx'));  //allowed extensions list//
    $uploader->setMaxSize(3);                          //set max file size to be allowed in MB//

    if($uploader->uploadFile('cl')){   //txtFile is the filebrowse element name //
        $cl  =   $uploader->getUploadName(); //get uploaded file name, renames on upload//

    }else{ //upload failed
        $fileError = $uploader->getMessage(); //get upload error message
        $error[] = array('file' => $fileError);
    }

    $cl = baseUrl() . "cv-uploads/" . $cl;


if(!empty($error)){
    $_SESSION['error'] = $error;
    $_SESSION['post'] = $_POST;
    header("Location: " . baseUrl() . "help/teams-and-roles/teams-and-roles/#form-start");
    die();
}

mysql_query("INSERT INTO teams_and_roles_form(job_ref,name,email,cv) VALUES('". mysql_real_escape_string($_POST['position']) ."','". mysql_real_escape_string($_POST['name']) ."','". mysql_real_escape_string($_POST['email']) ."','". $cv ."')") or die(mysql_error());

$mandrill = new Mandrill('KouNivonxJEpldwLc6CGcA');

/** ****************************
  * **** FLOW STAFF EMAIL   ****
  * ***************************/

$htmlMessage = $_POST['name'] . " ( " . $_POST['email'] . " ) has applied for the " . $_POST['position'] . " position. You can view their CV here: <a href='".$cv."'>".$cv."</a>";

$htmlMessage.= " and covering letter here:<a href='".$cl."'>".$cl."</a>";

$message = array(
    'subject' => 'Job application',
    'from_email' => 'register@flowenergy.uk.com',
    'from_name' => "Flow Energy",
    'html' => $htmlMessage,

    'to' => array(array('email' => "tokaty.whiffin@flowenergy.uk.com", 'name' => "Flow"))
    );
$mandrill->messages->send($message);


$_SESSION['success'] = true;
header("Location: " . baseUrl() . "help/teams-and-roles/teams-and-roles/#form-start");
