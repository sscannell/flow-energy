
<?php snippet('header') ?>
<div class="content">
<?php
 // Check if the "from" input field is filled out
  if (isset($_POST["email"]))
    {
    
	$from = $_POST["email"]; // sender
    $subject = "Enquiry from the Flow Energy Website";
    $message = "First Name: " . $_POST["firstname"] . "\r\n" . "Last Name: " . $_POST["lastname"] . "\r\n" . "Email Address: " . $_POST["email"]  . "\r\n" . "Account Number: " . $_POST['accountnumber'] . "\r\n\r\n" . "Message:" . "\r\n" . $_POST["message"];
    // message lines should not exceed 70 characters (PHP rule), so wrap it
    $message = wordwrap($message, 70);
    // send mail
    mail("help@flowenergy.uk.com",$subject,$message,"From: $from\n");
    echo "
	
	<h1>Message Sent!</h1>
	<p>Thank you for sending us your enquiry, we will be in touch as soon as possible.</p>
	
	";
    } else {
		
	
	echo "
	
	<h1>Sending Error!</h1>
	<p>Unfortunately there was a problem sending your email, please <a href='#'>click here</a> to return to the contact form and try again.</p>
	
	";	
		
	}
  
?>
</div>

<div class="clear"><!-- --></div>
<?php snippet('footer') ?>

</body>
</html>